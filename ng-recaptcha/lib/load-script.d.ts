/// <reference types="grecaptcha" />
declare global {
    interface Window {
        ng2recaptchaloaded: () => void;
    }
}
declare function loadScript(renderMode: "explicit" | string, onLoaded: (grecaptcha: ReCaptchaV2.ReCaptcha) => void, urlParams: string, url?: string, nonce?: string): void;
export declare const loader: {
    loadScript: typeof loadScript;
};
export {};
