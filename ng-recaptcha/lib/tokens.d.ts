import { InjectionToken } from "@angular/core";
import { RecaptchaSettings } from "./recaptcha-settings";
export declare const RECAPTCHA_LANGUAGE: InjectionToken<string>;
export declare const RECAPTCHA_BASE_URL: InjectionToken<string>;
export declare const RECAPTCHA_NONCE: InjectionToken<string>;
export declare const RECAPTCHA_SETTINGS: InjectionToken<RecaptchaSettings>;
export declare const RECAPTCHA_V3_SITE_KEY: InjectionToken<string>;
