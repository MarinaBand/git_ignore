import { isPlatformBrowser } from "@angular/common";
import { Inject, Injectable, Optional, PLATFORM_ID } from "@angular/core";
import { BehaviorSubject, of } from "rxjs";
import { loader } from "./load-script";
import { RECAPTCHA_BASE_URL, RECAPTCHA_LANGUAGE, RECAPTCHA_NONCE, RECAPTCHA_V3_SITE_KEY, } from "./tokens";
import * as i0 from "@angular/core";
export class RecaptchaLoaderService {
    constructor(
    // eslint-disable-next-line @typescript-eslint/ban-types
    platformId, language, baseUrl, nonce, v3SiteKey) {
        this.platformId = platformId;
        this.language = language;
        this.baseUrl = baseUrl;
        this.nonce = nonce;
        this.v3SiteKey = v3SiteKey;
        this.init();
        this.ready = isPlatformBrowser(this.platformId)
            ? RecaptchaLoaderService.ready.asObservable()
            : of();
    }
    /** @internal */
    init() {
        if (RecaptchaLoaderService.ready) {
            return;
        }
        if (isPlatformBrowser(this.platformId)) {
            const subject = new BehaviorSubject(null);
            RecaptchaLoaderService.ready = subject;
            const langParam = this.language ? "&hl=" + this.language : "";
            const renderMode = this.v3SiteKey || "explicit";
            loader.loadScript(renderMode, (grecaptcha) => subject.next(grecaptcha), langParam, this.baseUrl, this.nonce);
        }
    }
}
/**
 * @internal
 * @nocollapse
 */
RecaptchaLoaderService.ready = null;
RecaptchaLoaderService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaLoaderService, deps: [{ token: PLATFORM_ID }, { token: RECAPTCHA_LANGUAGE, optional: true }, { token: RECAPTCHA_BASE_URL, optional: true }, { token: RECAPTCHA_NONCE, optional: true }, { token: RECAPTCHA_V3_SITE_KEY, optional: true }], target: i0.ɵɵFactoryTarget.Injectable });
RecaptchaLoaderService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaLoaderService });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaLoaderService, decorators: [{
            type: Injectable
        }], ctorParameters: function () { return [{ type: Object, decorators: [{
                    type: Inject,
                    args: [PLATFORM_ID]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_LANGUAGE]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_BASE_URL]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_NONCE]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_V3_SITE_KEY]
                }] }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLWxvYWRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvbmctcmVjYXB0Y2hhL3NyYy9saWIvcmVjYXB0Y2hhLWxvYWRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUUsT0FBTyxFQUFFLGVBQWUsRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFdkQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQ0wsa0JBQWtCLEVBQ2xCLGtCQUFrQixFQUNsQixlQUFlLEVBQ2YscUJBQXFCLEdBQ3RCLE1BQU0sVUFBVSxDQUFDOztBQUdsQixNQUFNLE9BQU8sc0JBQXNCO0lBa0JqQztJQUNFLHdEQUF3RDtJQUNsQixVQUFrQixFQUNoQixRQUFpQixFQUNqQixPQUFnQixFQUNuQixLQUFjLEVBQ1IsU0FBa0I7UUFKdkIsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQU14RCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDN0MsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7WUFDN0MsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDO0lBQ1gsQ0FBQztJQUVELGdCQUFnQjtJQUNSLElBQUk7UUFDVixJQUFJLHNCQUFzQixDQUFDLEtBQUssRUFBRTtZQUNoQyxPQUFPO1NBQ1I7UUFDRCxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUN0QyxNQUFNLE9BQU8sR0FBRyxJQUFJLGVBQWUsQ0FBd0IsSUFBSSxDQUFDLENBQUM7WUFDakUsc0JBQXNCLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztZQUN2QyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBRTlELE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksVUFBVSxDQUFDO1lBQ2hELE1BQU0sQ0FBQyxVQUFVLENBQ2YsVUFBVSxFQUNWLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUN4QyxTQUFTLEVBQ1QsSUFBSSxDQUFDLE9BQU8sRUFDWixJQUFJLENBQUMsS0FBSyxDQUNYLENBQUM7U0FDSDtJQUNILENBQUM7O0FBdEREOzs7R0FHRztBQUNZLDRCQUFLLEdBQTJDLElBQUssQ0FBQTttSEFMekQsc0JBQXNCLGtCQW9CdkIsV0FBVyxhQUNDLGtCQUFrQiw2QkFDbEIsa0JBQWtCLDZCQUNsQixlQUFlLDZCQUNmLHFCQUFxQjt1SEF4QmhDLHNCQUFzQjsyRkFBdEIsc0JBQXNCO2tCQURsQyxVQUFVOzBEQXFCMkMsTUFBTTswQkFBdkQsTUFBTTsyQkFBQyxXQUFXOzswQkFDbEIsUUFBUTs7MEJBQUksTUFBTTsyQkFBQyxrQkFBa0I7OzBCQUNyQyxRQUFROzswQkFBSSxNQUFNOzJCQUFDLGtCQUFrQjs7MEJBQ3JDLFFBQVE7OzBCQUFJLE1BQU07MkJBQUMsZUFBZTs7MEJBQ2xDLFFBQVE7OzBCQUFJLE1BQU07MkJBQUMscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgaXNQbGF0Zm9ybUJyb3dzZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIE9wdGlvbmFsLCBQTEFURk9STV9JRCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUsIG9mIH0gZnJvbSBcInJ4anNcIjtcblxuaW1wb3J0IHsgbG9hZGVyIH0gZnJvbSBcIi4vbG9hZC1zY3JpcHRcIjtcbmltcG9ydCB7XG4gIFJFQ0FQVENIQV9CQVNFX1VSTCxcbiAgUkVDQVBUQ0hBX0xBTkdVQUdFLFxuICBSRUNBUFRDSEFfTk9OQ0UsXG4gIFJFQ0FQVENIQV9WM19TSVRFX0tFWSxcbn0gZnJvbSBcIi4vdG9rZW5zXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSZWNhcHRjaGFMb2FkZXJTZXJ2aWNlIHtcbiAgLyoqXG4gICAqIEBpbnRlcm5hbFxuICAgKiBAbm9jb2xsYXBzZVxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgcmVhZHk6IEJlaGF2aW9yU3ViamVjdDxSZUNhcHRjaGFWMi5SZUNhcHRjaGE+ID0gbnVsbDtcblxuICBwdWJsaWMgcmVhZHk6IE9ic2VydmFibGU8UmVDYXB0Y2hhVjIuUmVDYXB0Y2hhPjtcblxuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgbGFuZ3VhZ2U6IHN0cmluZztcbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIGJhc2VVcmw6IHN0cmluZztcbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIG5vbmNlOiBzdHJpbmc7XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSB2M1NpdGVLZXk6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L2Jhbi10eXBlc1xuICAgIEBJbmplY3QoUExBVEZPUk1fSUQpIHByaXZhdGUgcmVhZG9ubHkgcGxhdGZvcm1JZDogT2JqZWN0LFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoUkVDQVBUQ0hBX0xBTkdVQUdFKSBsYW5ndWFnZT86IHN0cmluZyxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KFJFQ0FQVENIQV9CQVNFX1VSTCkgYmFzZVVybD86IHN0cmluZyxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KFJFQ0FQVENIQV9OT05DRSkgbm9uY2U/OiBzdHJpbmcsXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChSRUNBUFRDSEFfVjNfU0lURV9LRVkpIHYzU2l0ZUtleT86IHN0cmluZ1xuICApIHtcbiAgICB0aGlzLmxhbmd1YWdlID0gbGFuZ3VhZ2U7XG4gICAgdGhpcy5iYXNlVXJsID0gYmFzZVVybDtcbiAgICB0aGlzLm5vbmNlID0gbm9uY2U7XG4gICAgdGhpcy52M1NpdGVLZXkgPSB2M1NpdGVLZXk7XG4gICAgdGhpcy5pbml0KCk7XG4gICAgdGhpcy5yZWFkeSA9IGlzUGxhdGZvcm1Ccm93c2VyKHRoaXMucGxhdGZvcm1JZClcbiAgICAgID8gUmVjYXB0Y2hhTG9hZGVyU2VydmljZS5yZWFkeS5hc09ic2VydmFibGUoKVxuICAgICAgOiBvZigpO1xuICB9XG5cbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIGluaXQoKSB7XG4gICAgaWYgKFJlY2FwdGNoYUxvYWRlclNlcnZpY2UucmVhZHkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaWYgKGlzUGxhdGZvcm1Ccm93c2VyKHRoaXMucGxhdGZvcm1JZCkpIHtcbiAgICAgIGNvbnN0IHN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PFJlQ2FwdGNoYVYyLlJlQ2FwdGNoYT4obnVsbCk7XG4gICAgICBSZWNhcHRjaGFMb2FkZXJTZXJ2aWNlLnJlYWR5ID0gc3ViamVjdDtcbiAgICAgIGNvbnN0IGxhbmdQYXJhbSA9IHRoaXMubGFuZ3VhZ2UgPyBcIiZobD1cIiArIHRoaXMubGFuZ3VhZ2UgOiBcIlwiO1xuXG4gICAgICBjb25zdCByZW5kZXJNb2RlID0gdGhpcy52M1NpdGVLZXkgfHwgXCJleHBsaWNpdFwiO1xuICAgICAgbG9hZGVyLmxvYWRTY3JpcHQoXG4gICAgICAgIHJlbmRlck1vZGUsXG4gICAgICAgIChncmVjYXB0Y2hhKSA9PiBzdWJqZWN0Lm5leHQoZ3JlY2FwdGNoYSksXG4gICAgICAgIGxhbmdQYXJhbSxcbiAgICAgICAgdGhpcy5iYXNlVXJsLFxuICAgICAgICB0aGlzLm5vbmNlXG4gICAgICApO1xuICAgIH1cbiAgfVxufVxuIl19