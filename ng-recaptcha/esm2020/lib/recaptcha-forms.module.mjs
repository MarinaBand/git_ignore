import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RecaptchaCommonModule } from "./recaptcha-common.module";
import { RecaptchaValueAccessorDirective } from "./recaptcha-value-accessor.directive";
import * as i0 from "@angular/core";
export class RecaptchaFormsModule {
}
RecaptchaFormsModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaFormsModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
RecaptchaFormsModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaFormsModule, declarations: [RecaptchaValueAccessorDirective], imports: [FormsModule, RecaptchaCommonModule], exports: [RecaptchaValueAccessorDirective] });
RecaptchaFormsModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaFormsModule, imports: [[FormsModule, RecaptchaCommonModule]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaFormsModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [RecaptchaValueAccessorDirective],
                    exports: [RecaptchaValueAccessorDirective],
                    imports: [FormsModule, RecaptchaCommonModule],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLWZvcm1zLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL25nLXJlY2FwdGNoYS9zcmMvbGliL3JlY2FwdGNoYS1mb3Jtcy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFN0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDbEUsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7O0FBT3ZGLE1BQU0sT0FBTyxvQkFBb0I7O2lIQUFwQixvQkFBb0I7a0hBQXBCLG9CQUFvQixpQkFKaEIsK0JBQStCLGFBRXBDLFdBQVcsRUFBRSxxQkFBcUIsYUFEbEMsK0JBQStCO2tIQUc5QixvQkFBb0IsWUFGdEIsQ0FBQyxXQUFXLEVBQUUscUJBQXFCLENBQUM7MkZBRWxDLG9CQUFvQjtrQkFMaEMsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQztvQkFDL0MsT0FBTyxFQUFFLENBQUMsK0JBQStCLENBQUM7b0JBQzFDLE9BQU8sRUFBRSxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsQ0FBQztpQkFDOUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5pbXBvcnQgeyBSZWNhcHRjaGFDb21tb25Nb2R1bGUgfSBmcm9tIFwiLi9yZWNhcHRjaGEtY29tbW9uLm1vZHVsZVwiO1xuaW1wb3J0IHsgUmVjYXB0Y2hhVmFsdWVBY2Nlc3NvckRpcmVjdGl2ZSB9IGZyb20gXCIuL3JlY2FwdGNoYS12YWx1ZS1hY2Nlc3Nvci5kaXJlY3RpdmVcIjtcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbUmVjYXB0Y2hhVmFsdWVBY2Nlc3NvckRpcmVjdGl2ZV0sXG4gIGV4cG9ydHM6IFtSZWNhcHRjaGFWYWx1ZUFjY2Vzc29yRGlyZWN0aXZlXSxcbiAgaW1wb3J0czogW0Zvcm1zTW9kdWxlLCBSZWNhcHRjaGFDb21tb25Nb2R1bGVdLFxufSlcbmV4cG9ydCBjbGFzcyBSZWNhcHRjaGFGb3Jtc01vZHVsZSB7fVxuIl19