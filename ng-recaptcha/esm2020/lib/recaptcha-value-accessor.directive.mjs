import { Directive, forwardRef, HostListener } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "./recaptcha.component";
export class RecaptchaValueAccessorDirective {
    constructor(host) {
        this.host = host;
        this.requiresControllerReset = false;
    }
    writeValue(value) {
        if (!value) {
            this.host.reset();
        }
        else {
            // In this case, it is most likely that a form controller has requested to write a specific value into the component.
            // This isn't really a supported case - reCAPTCHA values are single-use, and, in a sense, readonly.
            // What this means is that the form controller has recaptcha control state of X, while reCAPTCHA itself can't "restore"
            // to that state. In order to make form controller aware of this discrepancy, and to fix the said misalignment,
            // we'll be telling the controller to "reset" the value back to null.
            if (this.host.__unsafe_widgetValue !== value &&
                Boolean(this.host.__unsafe_widgetValue) === false) {
                this.requiresControllerReset = true;
            }
        }
    }
    registerOnChange(fn) {
        this.onChange = fn;
        if (this.requiresControllerReset) {
            this.requiresControllerReset = false;
            this.onChange(null);
        }
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    onResolve($event) {
        if (this.onChange) {
            this.onChange($event);
        }
        if (this.onTouched) {
            this.onTouched();
        }
    }
}
RecaptchaValueAccessorDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaValueAccessorDirective, deps: [{ token: i1.RecaptchaComponent }], target: i0.ɵɵFactoryTarget.Directive });
RecaptchaValueAccessorDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.0", type: RecaptchaValueAccessorDirective, selector: "re-captcha[formControlName],re-captcha[formControl],re-captcha[ngModel]", host: { listeners: { "resolved": "onResolve($event)" } }, providers: [
        {
            multi: true,
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RecaptchaValueAccessorDirective),
        },
    ], ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaValueAccessorDirective, decorators: [{
            type: Directive,
            args: [{
                    providers: [
                        {
                            multi: true,
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(() => RecaptchaValueAccessorDirective),
                        },
                    ],
                    selector: "re-captcha[formControlName],re-captcha[formControl],re-captcha[ngModel]",
                }]
        }], ctorParameters: function () { return [{ type: i1.RecaptchaComponent }]; }, propDecorators: { onResolve: [{
                type: HostListener,
                args: ["resolved", ["$event"]]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLXZhbHVlLWFjY2Vzc29yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL25nLXJlY2FwdGNoYS9zcmMvbGliL3JlY2FwdGNoYS12YWx1ZS1hY2Nlc3Nvci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBZXpFLE1BQU0sT0FBTywrQkFBK0I7SUFTMUMsWUFBb0IsSUFBd0I7UUFBeEIsU0FBSSxHQUFKLElBQUksQ0FBb0I7UUFGcEMsNEJBQXVCLEdBQUcsS0FBSyxDQUFDO0lBRU8sQ0FBQztJQUV6QyxVQUFVLENBQUMsS0FBYTtRQUM3QixJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNuQjthQUFNO1lBQ0wscUhBQXFIO1lBQ3JILG1HQUFtRztZQUNuRyx1SEFBdUg7WUFDdkgsK0dBQStHO1lBQy9HLHFFQUFxRTtZQUNyRSxJQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEtBQUssS0FBSztnQkFDeEMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxLQUFLLEVBQ2pEO2dCQUNBLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7YUFDckM7U0FDRjtJQUNILENBQUM7SUFFTSxnQkFBZ0IsQ0FBQyxFQUEyQjtRQUNqRCxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUNoQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckI7SUFDSCxDQUFDO0lBQ00saUJBQWlCLENBQUMsRUFBYztRQUNyQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRTRDLFNBQVMsQ0FBQyxNQUFjO1FBQ25FLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUNsQjtJQUNILENBQUM7OzRIQS9DVSwrQkFBK0I7Z0hBQS9CLCtCQUErQiw0SkFWL0I7UUFDVDtZQUNFLEtBQUssRUFBRSxJQUFJO1lBQ1gsT0FBTyxFQUFFLGlCQUFpQjtZQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLCtCQUErQixDQUFDO1NBQy9EO0tBQ0Y7MkZBSVUsK0JBQStCO2tCQVgzQyxTQUFTO21CQUFDO29CQUNULFNBQVMsRUFBRTt3QkFDVDs0QkFDRSxLQUFLLEVBQUUsSUFBSTs0QkFDWCxPQUFPLEVBQUUsaUJBQWlCOzRCQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxnQ0FBZ0MsQ0FBQzt5QkFDL0Q7cUJBQ0Y7b0JBQ0QsUUFBUSxFQUNOLHlFQUF5RTtpQkFDNUU7eUdBeUM4QyxTQUFTO3NCQUFyRCxZQUFZO3VCQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgZm9yd2FyZFJlZiwgSG9zdExpc3RlbmVyIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5pbXBvcnQgeyBSZWNhcHRjaGFDb21wb25lbnQgfSBmcm9tIFwiLi9yZWNhcHRjaGEuY29tcG9uZW50XCI7XG5cbkBEaXJlY3RpdmUoe1xuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gUmVjYXB0Y2hhVmFsdWVBY2Nlc3NvckRpcmVjdGl2ZSksXG4gICAgfSxcbiAgXSxcbiAgc2VsZWN0b3I6XG4gICAgXCJyZS1jYXB0Y2hhW2Zvcm1Db250cm9sTmFtZV0scmUtY2FwdGNoYVtmb3JtQ29udHJvbF0scmUtY2FwdGNoYVtuZ01vZGVsXVwiLFxufSlcbmV4cG9ydCBjbGFzcyBSZWNhcHRjaGFWYWx1ZUFjY2Vzc29yRGlyZWN0aXZlIGltcGxlbWVudHMgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgb25DaGFuZ2U6ICh2YWx1ZTogc3RyaW5nKSA9PiB2b2lkO1xuXG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBvblRvdWNoZWQ6ICgpID0+IHZvaWQ7XG5cbiAgcHJpdmF0ZSByZXF1aXJlc0NvbnRyb2xsZXJSZXNldCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaG9zdDogUmVjYXB0Y2hhQ29tcG9uZW50KSB7fVxuXG4gIHB1YmxpYyB3cml0ZVZhbHVlKHZhbHVlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBpZiAoIXZhbHVlKSB7XG4gICAgICB0aGlzLmhvc3QucmVzZXQoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gSW4gdGhpcyBjYXNlLCBpdCBpcyBtb3N0IGxpa2VseSB0aGF0IGEgZm9ybSBjb250cm9sbGVyIGhhcyByZXF1ZXN0ZWQgdG8gd3JpdGUgYSBzcGVjaWZpYyB2YWx1ZSBpbnRvIHRoZSBjb21wb25lbnQuXG4gICAgICAvLyBUaGlzIGlzbid0IHJlYWxseSBhIHN1cHBvcnRlZCBjYXNlIC0gcmVDQVBUQ0hBIHZhbHVlcyBhcmUgc2luZ2xlLXVzZSwgYW5kLCBpbiBhIHNlbnNlLCByZWFkb25seS5cbiAgICAgIC8vIFdoYXQgdGhpcyBtZWFucyBpcyB0aGF0IHRoZSBmb3JtIGNvbnRyb2xsZXIgaGFzIHJlY2FwdGNoYSBjb250cm9sIHN0YXRlIG9mIFgsIHdoaWxlIHJlQ0FQVENIQSBpdHNlbGYgY2FuJ3QgXCJyZXN0b3JlXCJcbiAgICAgIC8vIHRvIHRoYXQgc3RhdGUuIEluIG9yZGVyIHRvIG1ha2UgZm9ybSBjb250cm9sbGVyIGF3YXJlIG9mIHRoaXMgZGlzY3JlcGFuY3ksIGFuZCB0byBmaXggdGhlIHNhaWQgbWlzYWxpZ25tZW50LFxuICAgICAgLy8gd2UnbGwgYmUgdGVsbGluZyB0aGUgY29udHJvbGxlciB0byBcInJlc2V0XCIgdGhlIHZhbHVlIGJhY2sgdG8gbnVsbC5cbiAgICAgIGlmIChcbiAgICAgICAgdGhpcy5ob3N0Ll9fdW5zYWZlX3dpZGdldFZhbHVlICE9PSB2YWx1ZSAmJlxuICAgICAgICBCb29sZWFuKHRoaXMuaG9zdC5fX3Vuc2FmZV93aWRnZXRWYWx1ZSkgPT09IGZhbHNlXG4gICAgICApIHtcbiAgICAgICAgdGhpcy5yZXF1aXJlc0NvbnRyb2xsZXJSZXNldCA9IHRydWU7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHJlZ2lzdGVyT25DaGFuZ2UoZm46ICh2YWx1ZTogc3RyaW5nKSA9PiB2b2lkKTogdm9pZCB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICAgIGlmICh0aGlzLnJlcXVpcmVzQ29udHJvbGxlclJlc2V0KSB7XG4gICAgICB0aGlzLnJlcXVpcmVzQ29udHJvbGxlclJlc2V0ID0gZmFsc2U7XG4gICAgICB0aGlzLm9uQ2hhbmdlKG51bGwpO1xuICAgIH1cbiAgfVxuICBwdWJsaWMgcmVnaXN0ZXJPblRvdWNoZWQoZm46ICgpID0+IHZvaWQpOiB2b2lkIHtcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcihcInJlc29sdmVkXCIsIFtcIiRldmVudFwiXSkgcHVibGljIG9uUmVzb2x2ZSgkZXZlbnQ6IHN0cmluZyk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9uQ2hhbmdlKSB7XG4gICAgICB0aGlzLm9uQ2hhbmdlKCRldmVudCk7XG4gICAgfVxuICAgIGlmICh0aGlzLm9uVG91Y2hlZCkge1xuICAgICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==