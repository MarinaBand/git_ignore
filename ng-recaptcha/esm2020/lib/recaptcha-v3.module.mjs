import { NgModule } from "@angular/core";
import { ReCaptchaV3Service } from "./recaptcha-v3.service";
import * as i0 from "@angular/core";
export class RecaptchaV3Module {
}
RecaptchaV3Module.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaV3Module, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
RecaptchaV3Module.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaV3Module });
RecaptchaV3Module.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaV3Module, providers: [ReCaptchaV3Service] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaV3Module, decorators: [{
            type: NgModule,
            args: [{
                    providers: [ReCaptchaV3Service],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLXYzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL25nLXJlY2FwdGNoYS9zcmMvbGliL3JlY2FwdGNoYS12My5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7QUFLNUQsTUFBTSxPQUFPLGlCQUFpQjs7OEdBQWpCLGlCQUFpQjsrR0FBakIsaUJBQWlCOytHQUFqQixpQkFBaUIsYUFGakIsQ0FBQyxrQkFBa0IsQ0FBQzsyRkFFcEIsaUJBQWlCO2tCQUg3QixRQUFRO21CQUFDO29CQUNSLFNBQVMsRUFBRSxDQUFDLGtCQUFrQixDQUFDO2lCQUNoQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgUmVDYXB0Y2hhVjNTZXJ2aWNlIH0gZnJvbSBcIi4vcmVjYXB0Y2hhLXYzLnNlcnZpY2VcIjtcblxuQE5nTW9kdWxlKHtcbiAgcHJvdmlkZXJzOiBbUmVDYXB0Y2hhVjNTZXJ2aWNlXSxcbn0pXG5leHBvcnQgY2xhc3MgUmVjYXB0Y2hhVjNNb2R1bGUge31cbiJdfQ==