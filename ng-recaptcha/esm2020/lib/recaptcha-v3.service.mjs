import { isPlatformBrowser } from "@angular/common";
import { Inject, Injectable, Optional, PLATFORM_ID, } from "@angular/core";
import { Subject } from "rxjs";
import { loader } from "./load-script";
import { RECAPTCHA_BASE_URL, RECAPTCHA_LANGUAGE, RECAPTCHA_NONCE, RECAPTCHA_V3_SITE_KEY, } from "./tokens";
import * as i0 from "@angular/core";
/**
 * The main service for working with reCAPTCHA v3 APIs.
 *
 * Use the `execute` method for executing a single action, and
 * `onExecute` observable for listening to all actions at once.
 */
export class ReCaptchaV3Service {
    constructor(zone, siteKey, 
    // eslint-disable-next-line @typescript-eslint/ban-types
    platformId, baseUrl, nonce, language) {
        /** @internal */
        this.onLoadComplete = (grecaptcha) => {
            this.grecaptcha = grecaptcha;
            if (this.actionBacklog && this.actionBacklog.length > 0) {
                this.actionBacklog.forEach(([action, subject]) => this.executeActionWithSubject(action, subject));
                this.actionBacklog = undefined;
            }
        };
        this.zone = zone;
        this.isBrowser = isPlatformBrowser(platformId);
        this.siteKey = siteKey;
        this.nonce = nonce;
        this.language = language;
        this.baseUrl = baseUrl;
        this.init();
    }
    get onExecute() {
        if (!this.onExecuteSubject) {
            this.onExecuteSubject = new Subject();
            this.onExecuteObservable = this.onExecuteSubject.asObservable();
        }
        return this.onExecuteObservable;
    }
    get onExecuteError() {
        if (!this.onExecuteErrorSubject) {
            this.onExecuteErrorSubject = new Subject();
            this.onExecuteErrorObservable = this.onExecuteErrorSubject.asObservable();
        }
        return this.onExecuteErrorObservable;
    }
    /**
     * Executes the provided `action` with reCAPTCHA v3 API.
     * Use the emitted token value for verification purposes on the backend.
     *
     * For more information about reCAPTCHA v3 actions and tokens refer to the official documentation at
     * https://developers.google.com/recaptcha/docs/v3.
     *
     * @param {string} action the action to execute
     * @returns {Observable<string>} an `Observable` that will emit the reCAPTCHA v3 string `token` value whenever ready.
     * The returned `Observable` completes immediately after emitting a value.
     */
    execute(action) {
        const subject = new Subject();
        if (this.isBrowser) {
            if (!this.grecaptcha) {
                // todo: add to array of later executions
                if (!this.actionBacklog) {
                    this.actionBacklog = [];
                }
                this.actionBacklog.push([action, subject]);
            }
            else {
                this.executeActionWithSubject(action, subject);
            }
        }
        return subject.asObservable();
    }
    /** @internal */
    executeActionWithSubject(action, subject) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const onError = (error) => {
            this.zone.run(() => {
                subject.error(error);
                if (this.onExecuteErrorSubject) {
                    // We don't know any better at this point, unfortunately, so have to resort to `any`
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    this.onExecuteErrorSubject.next({ action, error });
                }
            });
        };
        this.zone.runOutsideAngular(() => {
            try {
                this.grecaptcha
                    .execute(this.siteKey, { action })
                    .then((token) => {
                    this.zone.run(() => {
                        subject.next(token);
                        subject.complete();
                        if (this.onExecuteSubject) {
                            this.onExecuteSubject.next({ action, token });
                        }
                    });
                }, onError);
            }
            catch (e) {
                onError(e);
            }
        });
    }
    /** @internal */
    init() {
        if (this.isBrowser) {
            if ("grecaptcha" in window) {
                this.grecaptcha = grecaptcha;
            }
            else {
                const langParam = this.language ? "&hl=" + this.language : "";
                loader.loadScript(this.siteKey, this.onLoadComplete, langParam, this.baseUrl, this.nonce);
            }
        }
    }
}
ReCaptchaV3Service.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: ReCaptchaV3Service, deps: [{ token: i0.NgZone }, { token: RECAPTCHA_V3_SITE_KEY }, { token: PLATFORM_ID }, { token: RECAPTCHA_BASE_URL, optional: true }, { token: RECAPTCHA_NONCE, optional: true }, { token: RECAPTCHA_LANGUAGE, optional: true }], target: i0.ɵɵFactoryTarget.Injectable });
ReCaptchaV3Service.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: ReCaptchaV3Service });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: ReCaptchaV3Service, decorators: [{
            type: Injectable
        }], ctorParameters: function () { return [{ type: i0.NgZone }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [RECAPTCHA_V3_SITE_KEY]
                }] }, { type: Object, decorators: [{
                    type: Inject,
                    args: [PLATFORM_ID]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_BASE_URL]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_NONCE]
                }] }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_LANGUAGE]
                }] }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLXYzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9uZy1yZWNhcHRjaGEvc3JjL2xpYi9yZWNhcHRjaGEtdjMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNwRCxPQUFPLEVBQ0wsTUFBTSxFQUNOLFVBQVUsRUFFVixRQUFRLEVBQ1IsV0FBVyxHQUNaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBYyxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFM0MsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQ0wsa0JBQWtCLEVBQ2xCLGtCQUFrQixFQUNsQixlQUFlLEVBQ2YscUJBQXFCLEdBQ3RCLE1BQU0sVUFBVSxDQUFDOztBQTJCbEI7Ozs7O0dBS0c7QUFFSCxNQUFNLE9BQU8sa0JBQWtCO0lBMkI3QixZQUNFLElBQVksRUFDbUIsT0FBZTtJQUM5Qyx3REFBd0Q7SUFDbkMsVUFBa0IsRUFDQyxPQUFnQixFQUNuQixLQUFjLEVBQ1gsUUFBaUI7UUFpSDNELGdCQUFnQjtRQUNSLG1CQUFjLEdBQUcsQ0FBQyxVQUFpQyxFQUFFLEVBQUU7WUFDN0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7WUFDN0IsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsRUFBRSxFQUFFLENBQy9DLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQy9DLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7YUFDaEM7UUFDSCxDQUFDLENBQUM7UUF4SEEsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUV2QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsSUFBVyxTQUFTO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDMUIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksT0FBTyxFQUFpQixDQUFDO1lBQ3JELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDakU7UUFFRCxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNsQyxDQUFDO0lBRUQsSUFBVyxjQUFjO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDL0IsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksT0FBTyxFQUFzQixDQUFDO1lBQy9ELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDM0U7UUFFRCxPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUN2QyxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7T0FVRztJQUNJLE9BQU8sQ0FBQyxNQUFjO1FBQzNCLE1BQU0sT0FBTyxHQUFHLElBQUksT0FBTyxFQUFVLENBQUM7UUFDdEMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQix5Q0FBeUM7Z0JBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN2QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztpQkFDekI7Z0JBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQzthQUM1QztpQkFBTTtnQkFDTCxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ2hEO1NBQ0Y7UUFFRCxPQUFPLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsZ0JBQWdCO0lBQ1Isd0JBQXdCLENBQzlCLE1BQWMsRUFDZCxPQUF3QjtRQUV4Qiw4REFBOEQ7UUFDOUQsTUFBTSxPQUFPLEdBQUcsQ0FBQyxLQUFVLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2pCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO29CQUM5QixvRkFBb0Y7b0JBQ3BGLG1FQUFtRTtvQkFDbkUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2lCQUNwRDtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEVBQUU7WUFDL0IsSUFBSTtnQkFDRixJQUFJLENBQUMsVUFBVTtxQkFDWixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDO3FCQUNqQyxJQUFJLENBQUMsQ0FBQyxLQUFhLEVBQUUsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFO3dCQUNqQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNwQixPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ25CLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFOzRCQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7eUJBQy9DO29CQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNmO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1o7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxnQkFBZ0I7SUFDUixJQUFJO1FBQ1YsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksWUFBWSxJQUFJLE1BQU0sRUFBRTtnQkFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDOUQsTUFBTSxDQUFDLFVBQVUsQ0FDZixJQUFJLENBQUMsT0FBTyxFQUNaLElBQUksQ0FBQyxjQUFjLEVBQ25CLFNBQVMsRUFDVCxJQUFJLENBQUMsT0FBTyxFQUNaLElBQUksQ0FBQyxLQUFLLENBQ1gsQ0FBQzthQUNIO1NBQ0Y7SUFDSCxDQUFDOzsrR0FqSlUsa0JBQWtCLHdDQTZCbkIscUJBQXFCLGFBRXJCLFdBQVcsYUFDQyxrQkFBa0IsNkJBQ2xCLGVBQWUsNkJBQ2Ysa0JBQWtCO21IQWxDN0Isa0JBQWtCOzJGQUFsQixrQkFBa0I7a0JBRDlCLFVBQVU7OzBCQThCTixNQUFNOzJCQUFDLHFCQUFxQjs4QkFFSSxNQUFNOzBCQUF0QyxNQUFNOzJCQUFDLFdBQVc7OzBCQUNsQixRQUFROzswQkFBSSxNQUFNOzJCQUFDLGtCQUFrQjs7MEJBQ3JDLFFBQVE7OzBCQUFJLE1BQU07MkJBQUMsZUFBZTs7MEJBQ2xDLFFBQVE7OzBCQUFJLE1BQU07MkJBQUMsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgaXNQbGF0Zm9ybUJyb3dzZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XG5pbXBvcnQge1xuICBJbmplY3QsXG4gIEluamVjdGFibGUsXG4gIE5nWm9uZSxcbiAgT3B0aW9uYWwsXG4gIFBMQVRGT1JNX0lELFxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gXCJyeGpzXCI7XG5cbmltcG9ydCB7IGxvYWRlciB9IGZyb20gXCIuL2xvYWQtc2NyaXB0XCI7XG5pbXBvcnQge1xuICBSRUNBUFRDSEFfQkFTRV9VUkwsXG4gIFJFQ0FQVENIQV9MQU5HVUFHRSxcbiAgUkVDQVBUQ0hBX05PTkNFLFxuICBSRUNBUFRDSEFfVjNfU0lURV9LRVksXG59IGZyb20gXCIuL3Rva2Vuc1wiO1xuXG5leHBvcnQgaW50ZXJmYWNlIE9uRXhlY3V0ZURhdGEge1xuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGFjdGlvbiB0aGF0IGhhcyBiZWVuIGV4ZWN1dGVkLlxuICAgKi9cbiAgYWN0aW9uOiBzdHJpbmc7XG4gIC8qKlxuICAgKiBUaGUgdG9rZW4gdGhhdCByZUNBUFRDSEEgdjMgcHJvdmlkZWQgd2hlbiBleGVjdXRpbmcgdGhlIGFjdGlvbi5cbiAgICovXG4gIHRva2VuOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgT25FeGVjdXRlRXJyb3JEYXRhIHtcbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBhY3Rpb24gdGhhdCBoYXMgYmVlbiBleGVjdXRlZC5cbiAgICovXG4gIGFjdGlvbjogc3RyaW5nO1xuICAvKipcbiAgICogVGhlIGVycm9yIHdoaWNoIHdhcyBlbmNvdW50ZXJlZFxuICAgKi9cbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIEB0eXBlc2NyaXB0LWVzbGludC9uby1leHBsaWNpdC1hbnlcbiAgZXJyb3I6IGFueTtcbn1cblxudHlwZSBBY3Rpb25CYWNrbG9nRW50cnkgPSBbc3RyaW5nLCBTdWJqZWN0PHN0cmluZz5dO1xuXG4vKipcbiAqIFRoZSBtYWluIHNlcnZpY2UgZm9yIHdvcmtpbmcgd2l0aCByZUNBUFRDSEEgdjMgQVBJcy5cbiAqXG4gKiBVc2UgdGhlIGBleGVjdXRlYCBtZXRob2QgZm9yIGV4ZWN1dGluZyBhIHNpbmdsZSBhY3Rpb24sIGFuZFxuICogYG9uRXhlY3V0ZWAgb2JzZXJ2YWJsZSBmb3IgbGlzdGVuaW5nIHRvIGFsbCBhY3Rpb25zIGF0IG9uY2UuXG4gKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSZUNhcHRjaGFWM1NlcnZpY2Uge1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgcmVhZG9ubHkgaXNCcm93c2VyOiBib29sZWFuO1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgcmVhZG9ubHkgc2l0ZUtleTogc3RyaW5nO1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgcmVhZG9ubHkgem9uZTogTmdab25lO1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgYWN0aW9uQmFja2xvZzogQWN0aW9uQmFja2xvZ0VudHJ5W10gfCB1bmRlZmluZWQ7XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBub25jZTogc3RyaW5nO1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgbGFuZ3VhZ2U/OiBzdHJpbmc7XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBiYXNlVXJsOiBzdHJpbmc7XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBncmVjYXB0Y2hhOiBSZUNhcHRjaGFWMi5SZUNhcHRjaGE7XG5cbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIG9uRXhlY3V0ZVN1YmplY3Q6IFN1YmplY3Q8T25FeGVjdXRlRGF0YT47XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBvbkV4ZWN1dGVFcnJvclN1YmplY3Q6IFN1YmplY3Q8T25FeGVjdXRlRXJyb3JEYXRhPjtcbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIG9uRXhlY3V0ZU9ic2VydmFibGU6IE9ic2VydmFibGU8T25FeGVjdXRlRGF0YT47XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBvbkV4ZWN1dGVFcnJvck9ic2VydmFibGU6IE9ic2VydmFibGU8T25FeGVjdXRlRXJyb3JEYXRhPjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICB6b25lOiBOZ1pvbmUsXG4gICAgQEluamVjdChSRUNBUFRDSEFfVjNfU0lURV9LRVkpIHNpdGVLZXk6IHN0cmluZyxcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L2Jhbi10eXBlc1xuICAgIEBJbmplY3QoUExBVEZPUk1fSUQpIHBsYXRmb3JtSWQ6IE9iamVjdCxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KFJFQ0FQVENIQV9CQVNFX1VSTCkgYmFzZVVybD86IHN0cmluZyxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KFJFQ0FQVENIQV9OT05DRSkgbm9uY2U/OiBzdHJpbmcsXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChSRUNBUFRDSEFfTEFOR1VBR0UpIGxhbmd1YWdlPzogc3RyaW5nXG4gICkge1xuICAgIHRoaXMuem9uZSA9IHpvbmU7XG4gICAgdGhpcy5pc0Jyb3dzZXIgPSBpc1BsYXRmb3JtQnJvd3NlcihwbGF0Zm9ybUlkKTtcbiAgICB0aGlzLnNpdGVLZXkgPSBzaXRlS2V5O1xuICAgIHRoaXMubm9uY2UgPSBub25jZTtcbiAgICB0aGlzLmxhbmd1YWdlID0gbGFuZ3VhZ2U7XG4gICAgdGhpcy5iYXNlVXJsID0gYmFzZVVybDtcblxuICAgIHRoaXMuaW5pdCgpO1xuICB9XG5cbiAgcHVibGljIGdldCBvbkV4ZWN1dGUoKTogT2JzZXJ2YWJsZTxPbkV4ZWN1dGVEYXRhPiB7XG4gICAgaWYgKCF0aGlzLm9uRXhlY3V0ZVN1YmplY3QpIHtcbiAgICAgIHRoaXMub25FeGVjdXRlU3ViamVjdCA9IG5ldyBTdWJqZWN0PE9uRXhlY3V0ZURhdGE+KCk7XG4gICAgICB0aGlzLm9uRXhlY3V0ZU9ic2VydmFibGUgPSB0aGlzLm9uRXhlY3V0ZVN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMub25FeGVjdXRlT2JzZXJ2YWJsZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgb25FeGVjdXRlRXJyb3IoKTogT2JzZXJ2YWJsZTxPbkV4ZWN1dGVFcnJvckRhdGE+IHtcbiAgICBpZiAoIXRoaXMub25FeGVjdXRlRXJyb3JTdWJqZWN0KSB7XG4gICAgICB0aGlzLm9uRXhlY3V0ZUVycm9yU3ViamVjdCA9IG5ldyBTdWJqZWN0PE9uRXhlY3V0ZUVycm9yRGF0YT4oKTtcbiAgICAgIHRoaXMub25FeGVjdXRlRXJyb3JPYnNlcnZhYmxlID0gdGhpcy5vbkV4ZWN1dGVFcnJvclN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMub25FeGVjdXRlRXJyb3JPYnNlcnZhYmxlO1xuICB9XG5cbiAgLyoqXG4gICAqIEV4ZWN1dGVzIHRoZSBwcm92aWRlZCBgYWN0aW9uYCB3aXRoIHJlQ0FQVENIQSB2MyBBUEkuXG4gICAqIFVzZSB0aGUgZW1pdHRlZCB0b2tlbiB2YWx1ZSBmb3IgdmVyaWZpY2F0aW9uIHB1cnBvc2VzIG9uIHRoZSBiYWNrZW5kLlxuICAgKlxuICAgKiBGb3IgbW9yZSBpbmZvcm1hdGlvbiBhYm91dCByZUNBUFRDSEEgdjMgYWN0aW9ucyBhbmQgdG9rZW5zIHJlZmVyIHRvIHRoZSBvZmZpY2lhbCBkb2N1bWVudGF0aW9uIGF0XG4gICAqIGh0dHBzOi8vZGV2ZWxvcGVycy5nb29nbGUuY29tL3JlY2FwdGNoYS9kb2NzL3YzLlxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gYWN0aW9uIHRoZSBhY3Rpb24gdG8gZXhlY3V0ZVxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxzdHJpbmc+fSBhbiBgT2JzZXJ2YWJsZWAgdGhhdCB3aWxsIGVtaXQgdGhlIHJlQ0FQVENIQSB2MyBzdHJpbmcgYHRva2VuYCB2YWx1ZSB3aGVuZXZlciByZWFkeS5cbiAgICogVGhlIHJldHVybmVkIGBPYnNlcnZhYmxlYCBjb21wbGV0ZXMgaW1tZWRpYXRlbHkgYWZ0ZXIgZW1pdHRpbmcgYSB2YWx1ZS5cbiAgICovXG4gIHB1YmxpYyBleGVjdXRlKGFjdGlvbjogc3RyaW5nKTogT2JzZXJ2YWJsZTxzdHJpbmc+IHtcbiAgICBjb25zdCBzdWJqZWN0ID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xuICAgIGlmICh0aGlzLmlzQnJvd3Nlcikge1xuICAgICAgaWYgKCF0aGlzLmdyZWNhcHRjaGEpIHtcbiAgICAgICAgLy8gdG9kbzogYWRkIHRvIGFycmF5IG9mIGxhdGVyIGV4ZWN1dGlvbnNcbiAgICAgICAgaWYgKCF0aGlzLmFjdGlvbkJhY2tsb2cpIHtcbiAgICAgICAgICB0aGlzLmFjdGlvbkJhY2tsb2cgPSBbXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYWN0aW9uQmFja2xvZy5wdXNoKFthY3Rpb24sIHN1YmplY3RdKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZXhlY3V0ZUFjdGlvbldpdGhTdWJqZWN0KGFjdGlvbiwgc3ViamVjdCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG4gIH1cblxuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgZXhlY3V0ZUFjdGlvbldpdGhTdWJqZWN0KFxuICAgIGFjdGlvbjogc3RyaW5nLFxuICAgIHN1YmplY3Q6IFN1YmplY3Q8c3RyaW5nPlxuICApOiB2b2lkIHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLWV4cGxpY2l0LWFueVxuICAgIGNvbnN0IG9uRXJyb3IgPSAoZXJyb3I6IGFueSkgPT4ge1xuICAgICAgdGhpcy56b25lLnJ1bigoKSA9PiB7XG4gICAgICAgIHN1YmplY3QuZXJyb3IoZXJyb3IpO1xuICAgICAgICBpZiAodGhpcy5vbkV4ZWN1dGVFcnJvclN1YmplY3QpIHtcbiAgICAgICAgICAvLyBXZSBkb24ndCBrbm93IGFueSBiZXR0ZXIgYXQgdGhpcyBwb2ludCwgdW5mb3J0dW5hdGVseSwgc28gaGF2ZSB0byByZXNvcnQgdG8gYGFueWBcbiAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVuc2FmZS1hc3NpZ25tZW50XG4gICAgICAgICAgdGhpcy5vbkV4ZWN1dGVFcnJvclN1YmplY3QubmV4dCh7IGFjdGlvbiwgZXJyb3IgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICB0aGlzLnpvbmUucnVuT3V0c2lkZUFuZ3VsYXIoKCkgPT4ge1xuICAgICAgdHJ5IHtcbiAgICAgICAgdGhpcy5ncmVjYXB0Y2hhXG4gICAgICAgICAgLmV4ZWN1dGUodGhpcy5zaXRlS2V5LCB7IGFjdGlvbiB9KVxuICAgICAgICAgIC50aGVuKCh0b2tlbjogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnpvbmUucnVuKCgpID0+IHtcbiAgICAgICAgICAgICAgc3ViamVjdC5uZXh0KHRva2VuKTtcbiAgICAgICAgICAgICAgc3ViamVjdC5jb21wbGV0ZSgpO1xuICAgICAgICAgICAgICBpZiAodGhpcy5vbkV4ZWN1dGVTdWJqZWN0KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vbkV4ZWN1dGVTdWJqZWN0Lm5leHQoeyBhY3Rpb24sIHRva2VuIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9LCBvbkVycm9yKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgb25FcnJvcihlKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBpbml0KCkge1xuICAgIGlmICh0aGlzLmlzQnJvd3Nlcikge1xuICAgICAgaWYgKFwiZ3JlY2FwdGNoYVwiIGluIHdpbmRvdykge1xuICAgICAgICB0aGlzLmdyZWNhcHRjaGEgPSBncmVjYXB0Y2hhO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc3QgbGFuZ1BhcmFtID0gdGhpcy5sYW5ndWFnZSA/IFwiJmhsPVwiICsgdGhpcy5sYW5ndWFnZSA6IFwiXCI7XG4gICAgICAgIGxvYWRlci5sb2FkU2NyaXB0KFxuICAgICAgICAgIHRoaXMuc2l0ZUtleSxcbiAgICAgICAgICB0aGlzLm9uTG9hZENvbXBsZXRlLFxuICAgICAgICAgIGxhbmdQYXJhbSxcbiAgICAgICAgICB0aGlzLmJhc2VVcmwsXG4gICAgICAgICAgdGhpcy5ub25jZVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBvbkxvYWRDb21wbGV0ZSA9IChncmVjYXB0Y2hhOiBSZUNhcHRjaGFWMi5SZUNhcHRjaGEpID0+IHtcbiAgICB0aGlzLmdyZWNhcHRjaGEgPSBncmVjYXB0Y2hhO1xuICAgIGlmICh0aGlzLmFjdGlvbkJhY2tsb2cgJiYgdGhpcy5hY3Rpb25CYWNrbG9nLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuYWN0aW9uQmFja2xvZy5mb3JFYWNoKChbYWN0aW9uLCBzdWJqZWN0XSkgPT5cbiAgICAgICAgdGhpcy5leGVjdXRlQWN0aW9uV2l0aFN1YmplY3QoYWN0aW9uLCBzdWJqZWN0KVxuICAgICAgKTtcbiAgICAgIHRoaXMuYWN0aW9uQmFja2xvZyA9IHVuZGVmaW5lZDtcbiAgICB9XG4gIH07XG59XG4iXX0=