import { NgModule } from "@angular/core";
import { RecaptchaCommonModule } from "./recaptcha-common.module";
import { RecaptchaLoaderService } from "./recaptcha-loader.service";
import { RecaptchaComponent } from "./recaptcha.component";
import * as i0 from "@angular/core";
export class RecaptchaModule {
}
RecaptchaModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
RecaptchaModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaModule, imports: [RecaptchaCommonModule], exports: [RecaptchaComponent] });
RecaptchaModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaModule, providers: [RecaptchaLoaderService], imports: [[RecaptchaCommonModule]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaModule, decorators: [{
            type: NgModule,
            args: [{
                    exports: [RecaptchaComponent],
                    imports: [RecaptchaCommonModule],
                    providers: [RecaptchaLoaderService],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL25nLXJlY2FwdGNoYS9zcmMvbGliL3JlY2FwdGNoYS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQzs7QUFPM0QsTUFBTSxPQUFPLGVBQWU7OzRHQUFmLGVBQWU7NkdBQWYsZUFBZSxZQUhoQixxQkFBcUIsYUFEckIsa0JBQWtCOzZHQUlqQixlQUFlLGFBRmYsQ0FBQyxzQkFBc0IsQ0FBQyxZQUQxQixDQUFDLHFCQUFxQixDQUFDOzJGQUdyQixlQUFlO2tCQUwzQixRQUFRO21CQUFDO29CQUNSLE9BQU8sRUFBRSxDQUFDLGtCQUFrQixDQUFDO29CQUM3QixPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztvQkFDaEMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7aUJBQ3BDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBSZWNhcHRjaGFDb21tb25Nb2R1bGUgfSBmcm9tIFwiLi9yZWNhcHRjaGEtY29tbW9uLm1vZHVsZVwiO1xuaW1wb3J0IHsgUmVjYXB0Y2hhTG9hZGVyU2VydmljZSB9IGZyb20gXCIuL3JlY2FwdGNoYS1sb2FkZXIuc2VydmljZVwiO1xuaW1wb3J0IHsgUmVjYXB0Y2hhQ29tcG9uZW50IH0gZnJvbSBcIi4vcmVjYXB0Y2hhLmNvbXBvbmVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICBleHBvcnRzOiBbUmVjYXB0Y2hhQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1JlY2FwdGNoYUNvbW1vbk1vZHVsZV0sXG4gIHByb3ZpZGVyczogW1JlY2FwdGNoYUxvYWRlclNlcnZpY2VdLFxufSlcbmV4cG9ydCBjbGFzcyBSZWNhcHRjaGFNb2R1bGUge31cbiJdfQ==