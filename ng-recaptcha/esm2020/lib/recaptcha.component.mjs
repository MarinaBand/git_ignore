import { Component, EventEmitter, HostBinding, Inject, Input, Optional, Output, } from "@angular/core";
import { RECAPTCHA_SETTINGS } from "./tokens";
import * as i0 from "@angular/core";
import * as i1 from "./recaptcha-loader.service";
let nextId = 0;
export class RecaptchaComponent {
    constructor(elementRef, loader, zone, settings) {
        this.elementRef = elementRef;
        this.loader = loader;
        this.zone = zone;
        this.id = `ngrecaptcha-${nextId++}`;
        this.errorMode = "default";
        this.resolved = new EventEmitter();
        // The rename will happen a bit later
        // eslint-disable-next-line @angular-eslint/no-output-native
        this.error = new EventEmitter();
        if (settings) {
            this.siteKey = settings.siteKey;
            this.theme = settings.theme;
            this.type = settings.type;
            this.size = settings.size;
            this.badge = settings.badge;
        }
    }
    ngAfterViewInit() {
        this.subscription = this.loader.ready.subscribe((grecaptcha) => {
            if (grecaptcha != null && grecaptcha.render instanceof Function) {
                this.grecaptcha = grecaptcha;
                this.renderRecaptcha();
            }
        });
    }
    ngOnDestroy() {
        // reset the captcha to ensure it does not leave anything behind
        // after the component is no longer needed
        this.grecaptchaReset();
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * Executes the invisible recaptcha.
     * Does nothing if component's size is not set to "invisible".
     */
    execute() {
        if (this.size !== "invisible") {
            return;
        }
        if (this.widget != null) {
            this.grecaptcha.execute(this.widget);
        }
        else {
            // delay execution of recaptcha until it actually renders
            this.executeRequested = true;
        }
    }
    reset() {
        if (this.widget != null) {
            if (this.grecaptcha.getResponse(this.widget)) {
                // Only emit an event in case if something would actually change.
                // That way we do not trigger "touching" of the control if someone does a "reset"
                // on a non-resolved captcha.
                this.resolved.emit(null);
            }
            this.grecaptchaReset();
        }
    }
    /**
     * ⚠️ Warning! Use this property at your own risk!
     *
     * While this member is `public`, it is not a part of the component's public API.
     * The semantic versioning guarantees _will not be honored_! Thus, you might find that this property behavior changes in incompatible ways in minor or even patch releases.
     * You are **strongly advised** against using this property.
     * Instead, use more idiomatic ways to get reCAPTCHA value, such as `resolved` EventEmitter, or form-bound methods (ngModel, formControl, and the likes).å
     */
    get __unsafe_widgetValue() {
        return this.widget != null
            ? this.grecaptcha.getResponse(this.widget)
            : null;
    }
    /** @internal */
    expired() {
        this.resolved.emit(null);
    }
    /** @internal */
    errored(args) {
        this.error.emit(args);
    }
    /** @internal */
    captchaResponseCallback(response) {
        this.resolved.emit(response);
    }
    /** @internal */
    grecaptchaReset() {
        if (this.widget != null) {
            this.zone.runOutsideAngular(() => this.grecaptcha.reset(this.widget));
        }
    }
    /** @internal */
    renderRecaptcha() {
        // This `any` can be removed after @types/grecaptcha get updated
        const renderOptions = {
            badge: this.badge,
            callback: (response) => {
                this.zone.run(() => this.captchaResponseCallback(response));
            },
            "expired-callback": () => {
                this.zone.run(() => this.expired());
            },
            sitekey: this.siteKey,
            size: this.size,
            tabindex: this.tabIndex,
            theme: this.theme,
            type: this.type,
        };
        if (this.errorMode === "handled") {
            renderOptions["error-callback"] = (...args) => {
                this.zone.run(() => this.errored(args));
            };
        }
        this.widget = this.grecaptcha.render(this.elementRef.nativeElement, renderOptions);
        if (this.executeRequested === true) {
            this.executeRequested = false;
            this.execute();
        }
    }
}
RecaptchaComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaComponent, deps: [{ token: i0.ElementRef }, { token: i1.RecaptchaLoaderService }, { token: i0.NgZone }, { token: RECAPTCHA_SETTINGS, optional: true }], target: i0.ɵɵFactoryTarget.Component });
RecaptchaComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.0", type: RecaptchaComponent, selector: "re-captcha", inputs: { id: "id", siteKey: "siteKey", theme: "theme", type: "type", size: "size", tabIndex: "tabIndex", badge: "badge", errorMode: "errorMode" }, outputs: { resolved: "resolved", error: "error" }, host: { properties: { "attr.id": "this.id" } }, exportAs: ["reCaptcha"], ngImport: i0, template: ``, isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.0", ngImport: i0, type: RecaptchaComponent, decorators: [{
            type: Component,
            args: [{
                    exportAs: "reCaptcha",
                    selector: "re-captcha",
                    template: ``,
                }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i1.RecaptchaLoaderService }, { type: i0.NgZone }, { type: undefined, decorators: [{
                    type: Optional
                }, {
                    type: Inject,
                    args: [RECAPTCHA_SETTINGS]
                }] }]; }, propDecorators: { id: [{
                type: Input
            }, {
                type: HostBinding,
                args: ["attr.id"]
            }], siteKey: [{
                type: Input
            }], theme: [{
                type: Input
            }], type: [{
                type: Input
            }], size: [{
                type: Input
            }], tabIndex: [{
                type: Input
            }], badge: [{
                type: Input
            }], errorMode: [{
                type: Input
            }], resolved: [{
                type: Output
            }], error: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjYXB0Y2hhLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL25nLXJlY2FwdGNoYS9zcmMvbGliL3JlY2FwdGNoYS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUVMLFNBQVMsRUFFVCxZQUFZLEVBQ1osV0FBVyxFQUNYLE1BQU0sRUFDTixLQUFLLEVBR0wsUUFBUSxFQUNSLE1BQU0sR0FDUCxNQUFNLGVBQWUsQ0FBQztBQUt2QixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxVQUFVLENBQUM7OztBQUU5QyxJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUM7QUFhZixNQUFNLE9BQU8sa0JBQWtCO0lBMkI3QixZQUNVLFVBQW1DLEVBQ25DLE1BQThCLEVBQzlCLElBQVksRUFDb0IsUUFBNEI7UUFINUQsZUFBVSxHQUFWLFVBQVUsQ0FBeUI7UUFDbkMsV0FBTSxHQUFOLE1BQU0sQ0FBd0I7UUFDOUIsU0FBSSxHQUFKLElBQUksQ0FBUTtRQTNCZixPQUFFLEdBQUcsZUFBZSxNQUFNLEVBQUUsRUFBRSxDQUFDO1FBUXRCLGNBQVMsR0FBMEIsU0FBUyxDQUFDO1FBRTVDLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBQ3ZELHFDQUFxQztRQUNyQyw0REFBNEQ7UUFDM0MsVUFBSyxHQUFHLElBQUksWUFBWSxFQUE0QixDQUFDO1FBaUJwRSxJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUNoQyxJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7WUFDNUIsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUMxQixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7U0FDN0I7SUFDSCxDQUFDO0lBRU0sZUFBZTtRQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FDN0MsQ0FBQyxVQUFpQyxFQUFFLEVBQUU7WUFDcEMsSUFBSSxVQUFVLElBQUksSUFBSSxJQUFJLFVBQVUsQ0FBQyxNQUFNLFlBQVksUUFBUSxFQUFFO2dCQUMvRCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO1FBQ0gsQ0FBQyxDQUNGLENBQUM7SUFDSixDQUFDO0lBRU0sV0FBVztRQUNoQixnRUFBZ0U7UUFDaEUsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNqQztJQUNILENBQUM7SUFFRDs7O09BR0c7SUFDSSxPQUFPO1FBQ1osSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtZQUM3QixPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN0QzthQUFNO1lBQ0wseURBQXlEO1lBQ3pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDOUI7SUFDSCxDQUFDO0lBRU0sS0FBSztRQUNWLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzVDLGlFQUFpRTtnQkFDakUsaUZBQWlGO2dCQUNqRiw2QkFBNkI7Z0JBQzdCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCO1lBRUQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSCxJQUFXLG9CQUFvQjtRQUM3QixPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSTtZQUN4QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUMxQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ1gsQ0FBQztJQUVELGdCQUFnQjtJQUNSLE9BQU87UUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsZ0JBQWdCO0lBQ1IsT0FBTyxDQUFDLElBQThCO1FBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxnQkFBZ0I7SUFDUix1QkFBdUIsQ0FBQyxRQUFnQjtRQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsZ0JBQWdCO0lBQ1IsZUFBZTtRQUNyQixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDdkU7SUFDSCxDQUFDO0lBRUQsZ0JBQWdCO0lBQ1IsZUFBZTtRQUNyQixnRUFBZ0U7UUFDaEUsTUFBTSxhQUFhLEdBQTJCO1lBQzVDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixRQUFRLEVBQUUsQ0FBQyxRQUFnQixFQUFFLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzlELENBQUM7WUFDRCxrQkFBa0IsRUFBRSxHQUFHLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQ3RDLENBQUM7WUFDRCxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7U0FDaEIsQ0FBQztRQUVGLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7WUFDaEMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQThCLEVBQUUsRUFBRTtnQkFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFDLENBQUMsQ0FBQztTQUNIO1FBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQzdCLGFBQWEsQ0FDZCxDQUFDO1FBRUYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssSUFBSSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7WUFDOUIsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2hCO0lBQ0gsQ0FBQzs7K0dBaktVLGtCQUFrQix3R0ErQlAsa0JBQWtCO21HQS9CN0Isa0JBQWtCLGtVQUZuQixFQUFFOzJGQUVELGtCQUFrQjtrQkFMOUIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsV0FBVztvQkFDckIsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLFFBQVEsRUFBRSxFQUFFO2lCQUNiOzswQkFnQ0ksUUFBUTs7MEJBQUksTUFBTTsyQkFBQyxrQkFBa0I7NENBNUJqQyxFQUFFO3NCQUZSLEtBQUs7O3NCQUNMLFdBQVc7dUJBQUMsU0FBUztnQkFHTixPQUFPO3NCQUF0QixLQUFLO2dCQUNVLEtBQUs7c0JBQXBCLEtBQUs7Z0JBQ1UsSUFBSTtzQkFBbkIsS0FBSztnQkFDVSxJQUFJO3NCQUFuQixLQUFLO2dCQUNVLFFBQVE7c0JBQXZCLEtBQUs7Z0JBQ1UsS0FBSztzQkFBcEIsS0FBSztnQkFDVSxTQUFTO3NCQUF4QixLQUFLO2dCQUVXLFFBQVE7c0JBQXhCLE1BQU07Z0JBR1UsS0FBSztzQkFBckIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIEFmdGVyVmlld0luaXQsXG4gIENvbXBvbmVudCxcbiAgRWxlbWVudFJlZixcbiAgRXZlbnRFbWl0dGVyLFxuICBIb3N0QmluZGluZyxcbiAgSW5qZWN0LFxuICBJbnB1dCxcbiAgTmdab25lLFxuICBPbkRlc3Ryb3ksXG4gIE9wdGlvbmFsLFxuICBPdXRwdXQsXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tIFwicnhqc1wiO1xuXG5pbXBvcnQgeyBSZWNhcHRjaGFMb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4vcmVjYXB0Y2hhLWxvYWRlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBSZWNhcHRjaGFTZXR0aW5ncyB9IGZyb20gXCIuL3JlY2FwdGNoYS1zZXR0aW5nc1wiO1xuaW1wb3J0IHsgUkVDQVBUQ0hBX1NFVFRJTkdTIH0gZnJvbSBcIi4vdG9rZW5zXCI7XG5cbmxldCBuZXh0SWQgPSAwO1xuXG5leHBvcnQgdHlwZSBOZXZlclVuZGVmaW5lZDxUPiA9IFQgZXh0ZW5kcyB1bmRlZmluZWQgPyBuZXZlciA6IFQ7XG5cbmV4cG9ydCB0eXBlIFJlY2FwdGNoYUVycm9yUGFyYW1ldGVycyA9IFBhcmFtZXRlcnM8XG4gIE5ldmVyVW5kZWZpbmVkPFJlQ2FwdGNoYVYyLlBhcmFtZXRlcnNbXCJlcnJvci1jYWxsYmFja1wiXT5cbj47XG5cbkBDb21wb25lbnQoe1xuICBleHBvcnRBczogXCJyZUNhcHRjaGFcIixcbiAgc2VsZWN0b3I6IFwicmUtY2FwdGNoYVwiLFxuICB0ZW1wbGF0ZTogYGAsXG59KVxuZXhwb3J0IGNsYXNzIFJlY2FwdGNoYUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpXG4gIEBIb3N0QmluZGluZyhcImF0dHIuaWRcIilcbiAgcHVibGljIGlkID0gYG5ncmVjYXB0Y2hhLSR7bmV4dElkKyt9YDtcblxuICBASW5wdXQoKSBwdWJsaWMgc2l0ZUtleTogc3RyaW5nO1xuICBASW5wdXQoKSBwdWJsaWMgdGhlbWU6IFJlQ2FwdGNoYVYyLlRoZW1lO1xuICBASW5wdXQoKSBwdWJsaWMgdHlwZTogUmVDYXB0Y2hhVjIuVHlwZTtcbiAgQElucHV0KCkgcHVibGljIHNpemU6IFJlQ2FwdGNoYVYyLlNpemU7XG4gIEBJbnB1dCgpIHB1YmxpYyB0YWJJbmRleDogbnVtYmVyO1xuICBASW5wdXQoKSBwdWJsaWMgYmFkZ2U6IFJlQ2FwdGNoYVYyLkJhZGdlO1xuICBASW5wdXQoKSBwdWJsaWMgZXJyb3JNb2RlOiBcImhhbmRsZWRcIiB8IFwiZGVmYXVsdFwiID0gXCJkZWZhdWx0XCI7XG5cbiAgQE91dHB1dCgpIHB1YmxpYyByZXNvbHZlZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuICAvLyBUaGUgcmVuYW1lIHdpbGwgaGFwcGVuIGEgYml0IGxhdGVyXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBAYW5ndWxhci1lc2xpbnQvbm8tb3V0cHV0LW5hdGl2ZVxuICBAT3V0cHV0KCkgcHVibGljIGVycm9yID0gbmV3IEV2ZW50RW1pdHRlcjxSZWNhcHRjaGFFcnJvclBhcmFtZXRlcnM+KCk7XG5cbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgd2lkZ2V0OiBudW1iZXI7XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBncmVjYXB0Y2hhOiBSZUNhcHRjaGFWMi5SZUNhcHRjaGE7XG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgcHJpdmF0ZSBleGVjdXRlUmVxdWVzdGVkOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXG4gICAgcHJpdmF0ZSBsb2FkZXI6IFJlY2FwdGNoYUxvYWRlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSB6b25lOiBOZ1pvbmUsXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChSRUNBUFRDSEFfU0VUVElOR1MpIHNldHRpbmdzPzogUmVjYXB0Y2hhU2V0dGluZ3NcbiAgKSB7XG4gICAgaWYgKHNldHRpbmdzKSB7XG4gICAgICB0aGlzLnNpdGVLZXkgPSBzZXR0aW5ncy5zaXRlS2V5O1xuICAgICAgdGhpcy50aGVtZSA9IHNldHRpbmdzLnRoZW1lO1xuICAgICAgdGhpcy50eXBlID0gc2V0dGluZ3MudHlwZTtcbiAgICAgIHRoaXMuc2l6ZSA9IHNldHRpbmdzLnNpemU7XG4gICAgICB0aGlzLmJhZGdlID0gc2V0dGluZ3MuYmFkZ2U7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMubG9hZGVyLnJlYWR5LnN1YnNjcmliZShcbiAgICAgIChncmVjYXB0Y2hhOiBSZUNhcHRjaGFWMi5SZUNhcHRjaGEpID0+IHtcbiAgICAgICAgaWYgKGdyZWNhcHRjaGEgIT0gbnVsbCAmJiBncmVjYXB0Y2hhLnJlbmRlciBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XG4gICAgICAgICAgdGhpcy5ncmVjYXB0Y2hhID0gZ3JlY2FwdGNoYTtcbiAgICAgICAgICB0aGlzLnJlbmRlclJlY2FwdGNoYSgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAvLyByZXNldCB0aGUgY2FwdGNoYSB0byBlbnN1cmUgaXQgZG9lcyBub3QgbGVhdmUgYW55dGhpbmcgYmVoaW5kXG4gICAgLy8gYWZ0ZXIgdGhlIGNvbXBvbmVudCBpcyBubyBsb25nZXIgbmVlZGVkXG4gICAgdGhpcy5ncmVjYXB0Y2hhUmVzZXQoKTtcbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb24pIHtcbiAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEV4ZWN1dGVzIHRoZSBpbnZpc2libGUgcmVjYXB0Y2hhLlxuICAgKiBEb2VzIG5vdGhpbmcgaWYgY29tcG9uZW50J3Mgc2l6ZSBpcyBub3Qgc2V0IHRvIFwiaW52aXNpYmxlXCIuXG4gICAqL1xuICBwdWJsaWMgZXhlY3V0ZSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5zaXplICE9PSBcImludmlzaWJsZVwiKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHRoaXMud2lkZ2V0ICE9IG51bGwpIHtcbiAgICAgIHRoaXMuZ3JlY2FwdGNoYS5leGVjdXRlKHRoaXMud2lkZ2V0KTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gZGVsYXkgZXhlY3V0aW9uIG9mIHJlY2FwdGNoYSB1bnRpbCBpdCBhY3R1YWxseSByZW5kZXJzXG4gICAgICB0aGlzLmV4ZWN1dGVSZXF1ZXN0ZWQgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyByZXNldCgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy53aWRnZXQgIT0gbnVsbCkge1xuICAgICAgaWYgKHRoaXMuZ3JlY2FwdGNoYS5nZXRSZXNwb25zZSh0aGlzLndpZGdldCkpIHtcbiAgICAgICAgLy8gT25seSBlbWl0IGFuIGV2ZW50IGluIGNhc2UgaWYgc29tZXRoaW5nIHdvdWxkIGFjdHVhbGx5IGNoYW5nZS5cbiAgICAgICAgLy8gVGhhdCB3YXkgd2UgZG8gbm90IHRyaWdnZXIgXCJ0b3VjaGluZ1wiIG9mIHRoZSBjb250cm9sIGlmIHNvbWVvbmUgZG9lcyBhIFwicmVzZXRcIlxuICAgICAgICAvLyBvbiBhIG5vbi1yZXNvbHZlZCBjYXB0Y2hhLlxuICAgICAgICB0aGlzLnJlc29sdmVkLmVtaXQobnVsbCk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZ3JlY2FwdGNoYVJlc2V0KCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIOKaoO+4jyBXYXJuaW5nISBVc2UgdGhpcyBwcm9wZXJ0eSBhdCB5b3VyIG93biByaXNrIVxuICAgKlxuICAgKiBXaGlsZSB0aGlzIG1lbWJlciBpcyBgcHVibGljYCwgaXQgaXMgbm90IGEgcGFydCBvZiB0aGUgY29tcG9uZW50J3MgcHVibGljIEFQSS5cbiAgICogVGhlIHNlbWFudGljIHZlcnNpb25pbmcgZ3VhcmFudGVlcyBfd2lsbCBub3QgYmUgaG9ub3JlZF8hIFRodXMsIHlvdSBtaWdodCBmaW5kIHRoYXQgdGhpcyBwcm9wZXJ0eSBiZWhhdmlvciBjaGFuZ2VzIGluIGluY29tcGF0aWJsZSB3YXlzIGluIG1pbm9yIG9yIGV2ZW4gcGF0Y2ggcmVsZWFzZXMuXG4gICAqIFlvdSBhcmUgKipzdHJvbmdseSBhZHZpc2VkKiogYWdhaW5zdCB1c2luZyB0aGlzIHByb3BlcnR5LlxuICAgKiBJbnN0ZWFkLCB1c2UgbW9yZSBpZGlvbWF0aWMgd2F5cyB0byBnZXQgcmVDQVBUQ0hBIHZhbHVlLCBzdWNoIGFzIGByZXNvbHZlZGAgRXZlbnRFbWl0dGVyLCBvciBmb3JtLWJvdW5kIG1ldGhvZHMgKG5nTW9kZWwsIGZvcm1Db250cm9sLCBhbmQgdGhlIGxpa2VzKS7DpVxuICAgKi9cbiAgcHVibGljIGdldCBfX3Vuc2FmZV93aWRnZXRWYWx1ZSgpOiBzdHJpbmcgfCBudWxsIHtcbiAgICByZXR1cm4gdGhpcy53aWRnZXQgIT0gbnVsbFxuICAgICAgPyB0aGlzLmdyZWNhcHRjaGEuZ2V0UmVzcG9uc2UodGhpcy53aWRnZXQpXG4gICAgICA6IG51bGw7XG4gIH1cblxuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgZXhwaXJlZCgpIHtcbiAgICB0aGlzLnJlc29sdmVkLmVtaXQobnVsbCk7XG4gIH1cblxuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgZXJyb3JlZChhcmdzOiBSZWNhcHRjaGFFcnJvclBhcmFtZXRlcnMpIHtcbiAgICB0aGlzLmVycm9yLmVtaXQoYXJncyk7XG4gIH1cblxuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgY2FwdGNoYVJlc3BvbnNlQ2FsbGJhY2socmVzcG9uc2U6IHN0cmluZykge1xuICAgIHRoaXMucmVzb2x2ZWQuZW1pdChyZXNwb25zZSk7XG4gIH1cblxuICAvKiogQGludGVybmFsICovXG4gIHByaXZhdGUgZ3JlY2FwdGNoYVJlc2V0KCkge1xuICAgIGlmICh0aGlzLndpZGdldCAhPSBudWxsKSB7XG4gICAgICB0aGlzLnpvbmUucnVuT3V0c2lkZUFuZ3VsYXIoKCkgPT4gdGhpcy5ncmVjYXB0Y2hhLnJlc2V0KHRoaXMud2lkZ2V0KSk7XG4gICAgfVxuICB9XG5cbiAgLyoqIEBpbnRlcm5hbCAqL1xuICBwcml2YXRlIHJlbmRlclJlY2FwdGNoYSgpIHtcbiAgICAvLyBUaGlzIGBhbnlgIGNhbiBiZSByZW1vdmVkIGFmdGVyIEB0eXBlcy9ncmVjYXB0Y2hhIGdldCB1cGRhdGVkXG4gICAgY29uc3QgcmVuZGVyT3B0aW9uczogUmVDYXB0Y2hhVjIuUGFyYW1ldGVycyA9IHtcbiAgICAgIGJhZGdlOiB0aGlzLmJhZGdlLFxuICAgICAgY2FsbGJhY2s6IChyZXNwb25zZTogc3RyaW5nKSA9PiB7XG4gICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4gdGhpcy5jYXB0Y2hhUmVzcG9uc2VDYWxsYmFjayhyZXNwb25zZSkpO1xuICAgICAgfSxcbiAgICAgIFwiZXhwaXJlZC1jYWxsYmFja1wiOiAoKSA9PiB7XG4gICAgICAgIHRoaXMuem9uZS5ydW4oKCkgPT4gdGhpcy5leHBpcmVkKCkpO1xuICAgICAgfSxcbiAgICAgIHNpdGVrZXk6IHRoaXMuc2l0ZUtleSxcbiAgICAgIHNpemU6IHRoaXMuc2l6ZSxcbiAgICAgIHRhYmluZGV4OiB0aGlzLnRhYkluZGV4LFxuICAgICAgdGhlbWU6IHRoaXMudGhlbWUsXG4gICAgICB0eXBlOiB0aGlzLnR5cGUsXG4gICAgfTtcblxuICAgIGlmICh0aGlzLmVycm9yTW9kZSA9PT0gXCJoYW5kbGVkXCIpIHtcbiAgICAgIHJlbmRlck9wdGlvbnNbXCJlcnJvci1jYWxsYmFja1wiXSA9ICguLi5hcmdzOiBSZWNhcHRjaGFFcnJvclBhcmFtZXRlcnMpID0+IHtcbiAgICAgICAgdGhpcy56b25lLnJ1bigoKSA9PiB0aGlzLmVycm9yZWQoYXJncykpO1xuICAgICAgfTtcbiAgICB9XG5cbiAgICB0aGlzLndpZGdldCA9IHRoaXMuZ3JlY2FwdGNoYS5yZW5kZXIoXG4gICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudCxcbiAgICAgIHJlbmRlck9wdGlvbnNcbiAgICApO1xuXG4gICAgaWYgKHRoaXMuZXhlY3V0ZVJlcXVlc3RlZCA9PT0gdHJ1ZSkge1xuICAgICAgdGhpcy5leGVjdXRlUmVxdWVzdGVkID0gZmFsc2U7XG4gICAgICB0aGlzLmV4ZWN1dGUoKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==