/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './button';
import * as ɵngcc2 from '@angular/material/core';
export declare class MatButtonModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatButtonModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatButtonModule, [typeof ɵngcc1.MatButton, typeof ɵngcc1.MatAnchor], [typeof ɵngcc2.MatRippleModule, typeof ɵngcc2.MatCommonModule], [typeof ɵngcc1.MatButton, typeof ɵngcc1.MatAnchor, typeof ɵngcc2.MatCommonModule]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatButtonModule>;
}

//# sourceMappingURL=button-module.d.ts.map