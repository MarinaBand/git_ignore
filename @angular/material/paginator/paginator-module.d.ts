/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './paginator';
import * as ɵngcc2 from '@angular/common';
import * as ɵngcc3 from '@angular/material/button';
import * as ɵngcc4 from '@angular/material/select';
import * as ɵngcc5 from '@angular/material/tooltip';
import * as ɵngcc6 from '@angular/material/core';
export declare class MatPaginatorModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatPaginatorModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatPaginatorModule, [typeof ɵngcc1.MatPaginator], [typeof ɵngcc2.CommonModule, typeof ɵngcc3.MatButtonModule, typeof ɵngcc4.MatSelectModule, typeof ɵngcc5.MatTooltipModule, typeof ɵngcc6.MatCommonModule], [typeof ɵngcc1.MatPaginator]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatPaginatorModule>;
}

//# sourceMappingURL=paginator-module.d.ts.map