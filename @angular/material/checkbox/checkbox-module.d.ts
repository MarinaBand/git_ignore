/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/** This module is used by both original and MDC-based checkbox implementations. */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './checkbox-required-validator';
import * as ɵngcc2 from './checkbox';
import * as ɵngcc3 from '@angular/material/core';
import * as ɵngcc4 from '@angular/cdk/observers';
export declare class _MatCheckboxRequiredValidatorModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<_MatCheckboxRequiredValidatorModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<_MatCheckboxRequiredValidatorModule, [typeof ɵngcc1.MatCheckboxRequiredValidator], never, [typeof ɵngcc1.MatCheckboxRequiredValidator]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<_MatCheckboxRequiredValidatorModule>;
}
export declare class MatCheckboxModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatCheckboxModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatCheckboxModule, [typeof ɵngcc2.MatCheckbox], [typeof ɵngcc3.MatRippleModule, typeof ɵngcc3.MatCommonModule, typeof ɵngcc4.ObserversModule, typeof _MatCheckboxRequiredValidatorModule], [typeof ɵngcc2.MatCheckbox, typeof ɵngcc3.MatCommonModule, typeof _MatCheckboxRequiredValidatorModule]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatCheckboxModule>;
}

//# sourceMappingURL=checkbox-module.d.ts.map