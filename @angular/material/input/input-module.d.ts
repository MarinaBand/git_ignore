/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './input';
import * as ɵngcc2 from './autosize';
import * as ɵngcc3 from '@angular/cdk/text-field';
import * as ɵngcc4 from '@angular/material/form-field';
import * as ɵngcc5 from '@angular/material/core';
export declare class MatInputModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatInputModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatInputModule, [typeof ɵngcc1.MatInput, typeof ɵngcc2.MatTextareaAutosize], [typeof ɵngcc3.TextFieldModule, typeof ɵngcc4.MatFormFieldModule, typeof ɵngcc5.MatCommonModule], [typeof ɵngcc3.TextFieldModule, typeof ɵngcc4.MatFormFieldModule, typeof ɵngcc1.MatInput, typeof ɵngcc2.MatTextareaAutosize]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatInputModule>;
}

//# sourceMappingURL=input-module.d.ts.map