/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './tooltip';
import * as ɵngcc2 from '@angular/cdk/a11y';
import * as ɵngcc3 from '@angular/common';
import * as ɵngcc4 from '@angular/cdk/overlay';
import * as ɵngcc5 from '@angular/material/core';
import * as ɵngcc6 from '@angular/cdk/scrolling';
export declare class MatTooltipModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatTooltipModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatTooltipModule, [typeof ɵngcc1.MatTooltip, typeof ɵngcc1.TooltipComponent], [typeof ɵngcc2.A11yModule, typeof ɵngcc3.CommonModule, typeof ɵngcc4.OverlayModule, typeof ɵngcc5.MatCommonModule], [typeof ɵngcc1.MatTooltip, typeof ɵngcc1.TooltipComponent, typeof ɵngcc5.MatCommonModule, typeof ɵngcc6.CdkScrollableModule]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatTooltipModule>;
}

//# sourceMappingURL=tooltip-module.d.ts.map