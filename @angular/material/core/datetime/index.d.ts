/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@angular/cdk/platform';
export * from './date-adapter';
export * from './date-formats';
export * from './native-date-adapter';
export * from './native-date-formats';
export declare class NativeDateModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<NativeDateModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<NativeDateModule, never, [typeof ɵngcc1.PlatformModule], never>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<NativeDateModule>;
}
export declare class MatNativeDateModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatNativeDateModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatNativeDateModule, never, [typeof NativeDateModule], never>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatNativeDateModule>;
}

//# sourceMappingURL=index.d.ts.map