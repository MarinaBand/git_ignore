/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './sort';
import * as ɵngcc2 from './sort-header';
import * as ɵngcc3 from '@angular/common';
import * as ɵngcc4 from '@angular/material/core';
export declare class MatSortModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatSortModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatSortModule, [typeof ɵngcc1.MatSort, typeof ɵngcc2.MatSortHeader], [typeof ɵngcc3.CommonModule, typeof ɵngcc4.MatCommonModule], [typeof ɵngcc1.MatSort, typeof ɵngcc2.MatSortHeader]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatSortModule>;
}

//# sourceMappingURL=sort-module.d.ts.map