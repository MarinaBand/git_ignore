/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './error';
import * as ɵngcc2 from './form-field';
import * as ɵngcc3 from './hint';
import * as ɵngcc4 from './label';
import * as ɵngcc5 from './placeholder';
import * as ɵngcc6 from './prefix';
import * as ɵngcc7 from './suffix';
import * as ɵngcc8 from '@angular/common';
import * as ɵngcc9 from '@angular/material/core';
import * as ɵngcc10 from '@angular/cdk/observers';
export declare class MatFormFieldModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatFormFieldModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<MatFormFieldModule, [typeof ɵngcc1.MatError, typeof ɵngcc2.MatFormField, typeof ɵngcc3.MatHint, typeof ɵngcc4.MatLabel, typeof ɵngcc5.MatPlaceholder, typeof ɵngcc6.MatPrefix, typeof ɵngcc7.MatSuffix], [typeof ɵngcc8.CommonModule, typeof ɵngcc9.MatCommonModule, typeof ɵngcc10.ObserversModule], [typeof ɵngcc9.MatCommonModule, typeof ɵngcc1.MatError, typeof ɵngcc2.MatFormField, typeof ɵngcc3.MatHint, typeof ɵngcc4.MatLabel, typeof ɵngcc5.MatPlaceholder, typeof ɵngcc6.MatPrefix, typeof ɵngcc7.MatSuffix]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<MatFormFieldModule>;
}

//# sourceMappingURL=form-field-module.d.ts.map