/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { InjectionToken } from '@angular/core';
/**
 * Injection token that can be used to reference instances of `MatPrefix`. It serves as
 * alternative token to the actual `MatPrefix` class which could cause unnecessary
 * retention of the class and its directive metadata.
 */
import * as ɵngcc0 from '@angular/core';
export declare const MAT_PREFIX: InjectionToken<MatPrefix>;
/** Prefix to be placed in front of the form field. */
export declare class MatPrefix {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<MatPrefix, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDeclaration<MatPrefix, "[matPrefix]", never, {}, {}, never>;
}

//# sourceMappingURL=prefix.d.ts.map