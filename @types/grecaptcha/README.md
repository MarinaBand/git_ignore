# Installation
> `npm install --save @types/grecaptcha`

# Summary
This package contains type definitions for Google Recaptcha (https://www.google.com/recaptcha).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/grecaptcha.

### Additional Details
 * Last updated: Tue, 06 Jul 2021 20:33:08 GMT
 * Dependencies: none
 * Global values: `grecaptcha`

# Credits
These definitions were written by [Kristof Mattei](http://kristofmattei.be), [Martin Costello](https://martincostello.com/), [Ruslan Arkhipau](https://github.com/DethAriel), [Rafael Tavares](https://github.com/rafaeltavares), [Florian Rohrer](https://github.com/RohrerF), and [Veniamin Krol](https://github.com/vkrol).
