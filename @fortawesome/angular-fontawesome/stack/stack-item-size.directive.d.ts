import { OnChanges, SimpleChanges } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class FaStackItemSizeDirective implements OnChanges {
    /**
     * Specify whether icon inside {@link FaStackComponent} should be rendered in
     * regular size (1x) or as a larger icon (2x).
     */
    stackItemSize: '1x' | '2x';
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaStackItemSizeDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDeclaration<FaStackItemSizeDirective, "fa-icon[stackItemSize],fa-duotone-icon[stackItemSize]", never, { "stackItemSize": "stackItemSize"; "size": "size"; }, {}, never>;
}

//# sourceMappingURL=stack-item-size.directive.d.ts.map