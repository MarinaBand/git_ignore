import { IconDefinition, IconLookup, IconPrefix, IconProp } from '@fortawesome/fontawesome-svg-core';
/**
 * Normalizing icon spec.
 */
export declare const faNormalizeIconSpec: (iconSpec: IconProp | IconDefinition, defaultPrefix: IconPrefix) => IconLookup | IconDefinition;
