"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.iconPackVersion = exports.angularFontawesomeVersion = exports.svgCoreVersion = void 0;
exports.svgCoreVersion = '^1.2.35';
exports.angularFontawesomeVersion = '~0.9.0';
exports.iconPackVersion = '^5.15.3';
//# sourceMappingURL=versions.js.map