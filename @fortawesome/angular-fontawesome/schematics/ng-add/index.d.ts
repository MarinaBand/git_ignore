import { workspaces } from '@angular-devkit/core';
import { Rule } from '@angular-devkit/schematics';
import { Schema } from './schema';
export default function (options: Schema): Rule;
export declare function getProjectTargetOptions(project: workspaces.ProjectDefinition, buildTarget: string): Record<string, string | number | boolean | import("@angular-devkit/core").JsonArray | import("@angular-devkit/core").JsonObject | null | undefined>;
