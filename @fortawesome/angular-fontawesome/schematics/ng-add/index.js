"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProjectTargetOptions = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const tasks_1 = require("@angular-devkit/schematics/tasks");
const typescript_1 = require("@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
const change_1 = require("@schematics/angular/utility/change");
const dependencies_1 = require("@schematics/angular/utility/dependencies");
const ng_ast_utils_1 = require("@schematics/angular/utility/ng-ast-utils");
const workspace_1 = require("@schematics/angular/utility/workspace");
const versions_1 = require("./versions");
function default_1(options) {
    return schematics_1.chain([
        (tree, context) => {
            dependencies_1.addPackageJsonDependency(tree, {
                type: dependencies_1.NodeDependencyType.Default,
                name: '@fortawesome/fontawesome-svg-core',
                version: versions_1.svgCoreVersion,
            });
            dependencies_1.addPackageJsonDependency(tree, {
                type: dependencies_1.NodeDependencyType.Default,
                name: '@fortawesome/angular-fontawesome',
                version: versions_1.angularFontawesomeVersion,
            });
            const iconPackages = options.iconPackages != null ? options.iconPackages : ['free-solid'];
            for (const pack of iconPackages) {
                dependencies_1.addPackageJsonDependency(tree, {
                    type: dependencies_1.NodeDependencyType.Default,
                    name: `@fortawesome/${pack}-svg-icons`,
                    version: versions_1.iconPackVersion,
                });
            }
            context.addTask(new tasks_1.NodePackageInstallTask());
            return tree;
        },
        addModule(options),
    ]);
}
exports.default = default_1;
function addModule(options) {
    return (host, context) => __awaiter(this, void 0, void 0, function* () {
        var _a;
        const workspace = yield workspace_1.getWorkspace(host);
        const projectName = (_a = options.project) !== null && _a !== void 0 ? _a : workspace.extensions.defaultProject;
        const project = workspace.projects.get(projectName);
        if (project == null) {
            throw new schematics_1.SchematicsException(`Project with name ${projectName} does not exist.`);
        }
        const buildOptions = getProjectTargetOptions(project, 'build');
        const modulePath = ng_ast_utils_1.getAppModulePath(host, buildOptions.main);
        const moduleSource = getSourceFile(host, modulePath);
        const changes = ast_utils_1.addImportToModule(moduleSource, modulePath, 'FontAwesomeModule', '@fortawesome/angular-fontawesome');
        const recorder = host.beginUpdate(modulePath);
        changes.forEach((change) => {
            if (change instanceof change_1.InsertChange) {
                recorder.insertLeft(change.pos, change.toAdd);
            }
        });
        host.commitUpdate(recorder);
        /* tslint is required to add a tslint fix task */
        try {
            require('tslint');
            context.addTask(new tasks_1.TslintFixTask(modulePath, {}));
        }
        catch (err) {
            context.logger.warn('Formatting was skipped because tslint is not installed.');
        }
    });
}
function getSourceFile(host, path) {
    const buffer = host.read(path);
    if (!buffer) {
        throw new schematics_1.SchematicsException(`Could not find ${path}.`);
    }
    const content = buffer.toString('utf-8');
    return typescript_1.createSourceFile(path, content, typescript_1.ScriptTarget.Latest, true);
}
function getProjectTargetOptions(project, buildTarget) {
    const buildTargetObject = project.targets.get(buildTarget);
    if (buildTargetObject && buildTargetObject.options) {
        return buildTargetObject.options;
    }
    throw new schematics_1.SchematicsException(`Cannot determine project target configuration for: ${buildTarget}.`);
}
exports.getProjectTargetOptions = getProjectTargetOptions;
//# sourceMappingURL=index.js.map