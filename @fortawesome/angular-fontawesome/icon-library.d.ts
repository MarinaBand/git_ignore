import { IconDefinition, IconName, IconPack, IconPrefix } from '@fortawesome/fontawesome-svg-core';
import * as ɵngcc0 from '@angular/core';
export interface FaIconLibraryInterface {
    addIcons(...icons: IconDefinition[]): void;
    addIconPacks(...packs: IconPack[]): void;
    getIconDefinition(prefix: IconPrefix, name: IconName): IconDefinition | null;
}
export declare class FaIconLibrary implements FaIconLibraryInterface {
    private definitions;
    addIcons(...icons: IconDefinition[]): void;
    addIconPacks(...packs: IconPack[]): void;
    getIconDefinition(prefix: IconPrefix, name: IconName): IconDefinition | null;
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaIconLibrary, never>;
}

//# sourceMappingURL=icon-library.d.ts.map