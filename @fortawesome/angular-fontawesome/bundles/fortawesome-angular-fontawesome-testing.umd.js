(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@fortawesome/angular-fontawesome')) :
    typeof define === 'function' && define.amd ? define('@fortawesome/angular-fontawesome/testing', ['exports', '@angular/core', '@fortawesome/angular-fontawesome'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory((global.fortawesome = global.fortawesome || {}, global.fortawesome['angular-fontawesome'] = global.fortawesome['angular-fontawesome'] || {}, global.fortawesome['angular-fontawesome'].testing = {}), global.ng.core, global.fortawesome['angular-fontawesome']));
}(this, (function (exports, i0, angularFontawesome) { 'use strict';

    function _interopNamespace(e) {
        if (e && e.__esModule) return e;
        var n = Object.create(null);
        if (e) {
            Object.keys(e).forEach(function (k) {
                if (k !== 'default') {
                    var d = Object.getOwnPropertyDescriptor(e, k);
                    Object.defineProperty(n, k, d.get ? d : {
                        enumerable: true,
                        get: function () {
                            return e[k];
                        }
                    });
                }
            });
        }
        n['default'] = e;
        return Object.freeze(n);
    }

    var i0__namespace = /*#__PURE__*/_interopNamespace(i0);

    var dummyIcon = {
        prefix: 'fad',
        iconName: 'dummy',
        icon: [512, 512, [], 'f030', 'M50 50 H462 V462 H50 Z'],
    };
    var MockFaIconLibrary = /** @class */ (function () {
        function MockFaIconLibrary() {
        }
        MockFaIconLibrary.prototype.addIcons = function () {
            throw new Error('Attempt to add an icon to the MockFaIconLibrary.');
        };
        MockFaIconLibrary.prototype.addIconPacks = function () {
            throw new Error('Attempt to add an icon pack to the MockFaIconLibrary.');
        };
        MockFaIconLibrary.prototype.getIconDefinition = function (prefix, name) {
            return dummyIcon;
        };
        return MockFaIconLibrary;
    }());
    MockFaIconLibrary.ɵprov = i0__namespace.ɵɵdefineInjectable({ factory: function MockFaIconLibrary_Factory() { return new MockFaIconLibrary(); }, token: MockFaIconLibrary, providedIn: "root" });
    MockFaIconLibrary.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root',
                },] }
    ];

    var FontAwesomeTestingModule = /** @class */ (function () {
        function FontAwesomeTestingModule() {
        }
        return FontAwesomeTestingModule;
    }());
    FontAwesomeTestingModule.decorators = [
        { type: i0.NgModule, args: [{
                    exports: [angularFontawesome.FontAwesomeModule],
                    providers: [{ provide: angularFontawesome.FaIconLibrary, useExisting: MockFaIconLibrary }],
                },] }
    ];

    /**
     * Generated bundle index. Do not edit.
     */

    exports.FontAwesomeTestingModule = FontAwesomeTestingModule;
    exports.ɵa = MockFaIconLibrary;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=fortawesome-angular-fontawesome-testing.umd.js.map
