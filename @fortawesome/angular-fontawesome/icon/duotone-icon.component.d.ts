import { IconDefinition, IconProp } from '@fortawesome/fontawesome-svg-core';
import { FaIconComponent } from './icon.component';
import * as ɵngcc0 from '@angular/core';
export declare class FaDuotoneIconComponent extends FaIconComponent {
    /**
     * Swap the default opacity of each duotone icon’s layers. This will make an
     * icon’s primary layer have the default opacity of 40% rather than its
     * secondary layer.
     *
     * @default false
     */
    swapOpacity?: 'true' | 'false' | boolean;
    /**
     * Customize the opacity of the primary icon layer.
     * Valid values are in range [0, 1.0].
     *
     * @default 1.0
     */
    primaryOpacity?: string | number;
    /**
     * Customize the opacity of the secondary icon layer.
     * Valid values are in range [0, 1.0].
     *
     * @default 0.4
     */
    secondaryOpacity?: string | number;
    /**
     * Customize the color of the primary icon layer.
     * Accepts any valid CSS color value.
     *
     * @default CSS inherited color
     */
    primaryColor?: string;
    /**
     * Customize the color of the secondary icon layer.
     * Accepts any valid CSS color value.
     *
     * @default CSS inherited color
     */
    secondaryColor?: string;
    protected findIconDefinition(i: IconProp | IconDefinition): IconDefinition | null;
    protected buildParams(): {
        title: string;
        transform: import("@fortawesome/fontawesome-svg-core").Transform;
        classes: string[];
        mask: IconDefinition;
        styles: import("@fortawesome/fontawesome-svg-core").Styles;
        symbol: import("@fortawesome/fontawesome-svg-core").FaSymbol;
        attributes: {
            role: string;
        };
    };
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaDuotoneIconComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDeclaration<FaDuotoneIconComponent, "fa-duotone-icon", never, { "swapOpacity": "swapOpacity"; "primaryOpacity": "primaryOpacity"; "secondaryOpacity": "secondaryOpacity"; "primaryColor": "primaryColor"; "secondaryColor": "secondaryColor"; }, {}, never, never>;
}

//# sourceMappingURL=duotone-icon.component.d.ts.map