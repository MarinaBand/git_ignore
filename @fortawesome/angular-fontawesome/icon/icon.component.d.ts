import { OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { FaSymbol, FlipProp, IconDefinition, IconProp, PullProp, RotateProp, SizeProp, Styles, Transform } from '@fortawesome/fontawesome-svg-core';
import { FaConfig } from '../config';
import { FaIconLibrary } from '../icon-library';
import { FaStackItemSizeDirective } from '../stack/stack-item-size.directive';
import { FaStackComponent } from '../stack/stack.component';
import * as ɵngcc0 from '@angular/core';
export declare class FaIconComponent implements OnChanges {
    private sanitizer;
    private config;
    private iconLibrary;
    private stackItem;
    icon: IconProp;
    /**
     * Specify a title for the icon.
     * This text will be displayed in a tooltip on hover and presented to the
     * screen readers.
     */
    title?: string;
    spin?: boolean;
    pulse?: boolean;
    mask?: IconProp;
    styles?: Styles;
    flip?: FlipProp;
    size?: SizeProp;
    pull?: PullProp;
    border?: boolean;
    inverse?: boolean;
    symbol?: FaSymbol;
    rotate?: RotateProp;
    fixedWidth?: boolean;
    classes?: string[];
    transform?: string | Transform;
    /**
     * Specify the `role` attribute for the rendered <svg> element.
     *
     * @default 'img'
     */
    a11yRole: string;
    renderedIconHTML: SafeHtml;
    constructor(sanitizer: DomSanitizer, config: FaConfig, iconLibrary: FaIconLibrary, stackItem: FaStackItemSizeDirective, stack: FaStackComponent);
    ngOnChanges(changes: SimpleChanges): never;
    /**
     * Programmatically trigger rendering of the icon.
     *
     * This method is useful, when creating {@link FaIconComponent} dynamically or
     * changing its inputs programmatically as in these cases icon won't be
     * re-rendered automatically.
     */
    render(): void;
    protected findIconDefinition(i: IconProp | IconDefinition): IconDefinition | null;
    protected buildParams(): {
        title: string;
        transform: Transform;
        classes: string[];
        mask: IconDefinition;
        styles: Styles;
        symbol: FaSymbol;
        attributes: {
            role: string;
        };
    };
    private renderIcon;
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaIconComponent, [null, null, null, { optional: true; }, { optional: true; }]>;
    static ɵcmp: ɵngcc0.ɵɵComponentDeclaration<FaIconComponent, "fa-icon", never, { "classes": "classes"; "icon": "icon"; "title": "title"; "spin": "spin"; "pulse": "pulse"; "mask": "mask"; "styles": "styles"; "flip": "flip"; "size": "size"; "pull": "pull"; "border": "border"; "inverse": "inverse"; "symbol": "symbol"; "rotate": "rotate"; "fixedWidth": "fixedWidth"; "transform": "transform"; "a11yRole": "a11yRole"; }, {}, never, never>;
}

//# sourceMappingURL=icon.component.d.ts.map