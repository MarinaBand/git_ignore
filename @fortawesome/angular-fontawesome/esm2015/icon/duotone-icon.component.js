import { Component, Input } from '@angular/core';
import { FaIconComponent } from './icon.component';
export class FaDuotoneIconComponent extends FaIconComponent {
    findIconDefinition(i) {
        const definition = super.findIconDefinition(i);
        if (definition != null && !Array.isArray(definition.icon[4])) {
            throw new Error('The specified icon does not appear to be a Duotone icon. ' +
                'Check that you specified the correct style: ' +
                `<fa-duotone-icon [icon]="['fad', '${definition.iconName}']"></fa-duotone-icon> ` +
                `or use: <fa-icon icon="${definition.iconName}"></fa-icon> instead.`);
        }
        return definition;
    }
    buildParams() {
        const params = super.buildParams();
        if (this.swapOpacity === true || this.swapOpacity === 'true') {
            params.classes.push('fa-swap-opacity');
        }
        if (this.primaryOpacity != null) {
            params.styles['--fa-primary-opacity'] = this.primaryOpacity.toString();
        }
        if (this.secondaryOpacity != null) {
            params.styles['--fa-secondary-opacity'] = this.secondaryOpacity.toString();
        }
        if (this.primaryColor != null) {
            params.styles['--fa-primary-color'] = this.primaryColor;
        }
        if (this.secondaryColor != null) {
            params.styles['--fa-secondary-color'] = this.secondaryColor;
        }
        return params;
    }
}
FaDuotoneIconComponent.decorators = [
    { type: Component, args: [{
                selector: 'fa-duotone-icon',
                template: ``
            },] }
];
FaDuotoneIconComponent.propDecorators = {
    swapOpacity: [{ type: Input }],
    primaryOpacity: [{ type: Input }],
    secondaryOpacity: [{ type: Input }],
    primaryColor: [{ type: Input }],
    secondaryColor: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHVvdG9uZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvaWNvbi9kdW90b25lLWljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQU1uRCxNQUFNLE9BQU8sc0JBQXVCLFNBQVEsZUFBZTtJQTBDL0Msa0JBQWtCLENBQUMsQ0FBNEI7UUFDdkQsTUFBTSxVQUFVLEdBQUcsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRS9DLElBQUksVUFBVSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVELE1BQU0sSUFBSSxLQUFLLENBQ2IsMkRBQTJEO2dCQUN6RCw4Q0FBOEM7Z0JBQzlDLHFDQUFxQyxVQUFVLENBQUMsUUFBUSx5QkFBeUI7Z0JBQ2pGLDBCQUEwQixVQUFVLENBQUMsUUFBUSx1QkFBdUIsQ0FDdkUsQ0FBQztTQUNIO1FBRUQsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVTLFdBQVc7UUFDbkIsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRW5DLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxNQUFNLEVBQUU7WUFDNUQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN4QztRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLEVBQUU7WUFDL0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDeEU7UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7WUFDakMsTUFBTSxDQUFDLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUM1RTtRQUNELElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLEVBQUU7WUFDN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDekQ7UUFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxFQUFFO1lBQy9CLE1BQU0sQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1NBQzdEO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7O1lBakZGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQixRQUFRLEVBQUUsRUFBRTthQUNiOzs7MEJBU0UsS0FBSzs2QkFRTCxLQUFLOytCQVFMLEtBQUs7MkJBUUwsS0FBSzs2QkFRTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSWNvbkRlZmluaXRpb24sIEljb25Qcm9wIH0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLXN2Zy1jb3JlJztcbmltcG9ydCB7IEZhSWNvbkNvbXBvbmVudCB9IGZyb20gJy4vaWNvbi5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYS1kdW90b25lLWljb24nLFxuICB0ZW1wbGF0ZTogYGAsXG59KVxuZXhwb3J0IGNsYXNzIEZhRHVvdG9uZUljb25Db21wb25lbnQgZXh0ZW5kcyBGYUljb25Db21wb25lbnQge1xuICAvKipcbiAgICogU3dhcCB0aGUgZGVmYXVsdCBvcGFjaXR5IG9mIGVhY2ggZHVvdG9uZSBpY29u4oCZcyBsYXllcnMuIFRoaXMgd2lsbCBtYWtlIGFuXG4gICAqIGljb27igJlzIHByaW1hcnkgbGF5ZXIgaGF2ZSB0aGUgZGVmYXVsdCBvcGFjaXR5IG9mIDQwJSByYXRoZXIgdGhhbiBpdHNcbiAgICogc2Vjb25kYXJ5IGxheWVyLlxuICAgKlxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKi9cbiAgQElucHV0KCkgc3dhcE9wYWNpdHk/OiAndHJ1ZScgfCAnZmFsc2UnIHwgYm9vbGVhbjtcblxuICAvKipcbiAgICogQ3VzdG9taXplIHRoZSBvcGFjaXR5IG9mIHRoZSBwcmltYXJ5IGljb24gbGF5ZXIuXG4gICAqIFZhbGlkIHZhbHVlcyBhcmUgaW4gcmFuZ2UgWzAsIDEuMF0uXG4gICAqXG4gICAqIEBkZWZhdWx0IDEuMFxuICAgKi9cbiAgQElucHV0KCkgcHJpbWFyeU9wYWNpdHk/OiBzdHJpbmcgfCBudW1iZXI7XG5cbiAgLyoqXG4gICAqIEN1c3RvbWl6ZSB0aGUgb3BhY2l0eSBvZiB0aGUgc2Vjb25kYXJ5IGljb24gbGF5ZXIuXG4gICAqIFZhbGlkIHZhbHVlcyBhcmUgaW4gcmFuZ2UgWzAsIDEuMF0uXG4gICAqXG4gICAqIEBkZWZhdWx0IDAuNFxuICAgKi9cbiAgQElucHV0KCkgc2Vjb25kYXJ5T3BhY2l0eT86IHN0cmluZyB8IG51bWJlcjtcblxuICAvKipcbiAgICogQ3VzdG9taXplIHRoZSBjb2xvciBvZiB0aGUgcHJpbWFyeSBpY29uIGxheWVyLlxuICAgKiBBY2NlcHRzIGFueSB2YWxpZCBDU1MgY29sb3IgdmFsdWUuXG4gICAqXG4gICAqIEBkZWZhdWx0IENTUyBpbmhlcml0ZWQgY29sb3JcbiAgICovXG4gIEBJbnB1dCgpIHByaW1hcnlDb2xvcj86IHN0cmluZztcblxuICAvKipcbiAgICogQ3VzdG9taXplIHRoZSBjb2xvciBvZiB0aGUgc2Vjb25kYXJ5IGljb24gbGF5ZXIuXG4gICAqIEFjY2VwdHMgYW55IHZhbGlkIENTUyBjb2xvciB2YWx1ZS5cbiAgICpcbiAgICogQGRlZmF1bHQgQ1NTIGluaGVyaXRlZCBjb2xvclxuICAgKi9cbiAgQElucHV0KCkgc2Vjb25kYXJ5Q29sb3I/OiBzdHJpbmc7XG5cbiAgcHJvdGVjdGVkIGZpbmRJY29uRGVmaW5pdGlvbihpOiBJY29uUHJvcCB8IEljb25EZWZpbml0aW9uKTogSWNvbkRlZmluaXRpb24gfCBudWxsIHtcbiAgICBjb25zdCBkZWZpbml0aW9uID0gc3VwZXIuZmluZEljb25EZWZpbml0aW9uKGkpO1xuXG4gICAgaWYgKGRlZmluaXRpb24gIT0gbnVsbCAmJiAhQXJyYXkuaXNBcnJheShkZWZpbml0aW9uLmljb25bNF0pKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICdUaGUgc3BlY2lmaWVkIGljb24gZG9lcyBub3QgYXBwZWFyIHRvIGJlIGEgRHVvdG9uZSBpY29uLiAnICtcbiAgICAgICAgICAnQ2hlY2sgdGhhdCB5b3Ugc3BlY2lmaWVkIHRoZSBjb3JyZWN0IHN0eWxlOiAnICtcbiAgICAgICAgICBgPGZhLWR1b3RvbmUtaWNvbiBbaWNvbl09XCJbJ2ZhZCcsICcke2RlZmluaXRpb24uaWNvbk5hbWV9J11cIj48L2ZhLWR1b3RvbmUtaWNvbj4gYCArXG4gICAgICAgICAgYG9yIHVzZTogPGZhLWljb24gaWNvbj1cIiR7ZGVmaW5pdGlvbi5pY29uTmFtZX1cIj48L2ZhLWljb24+IGluc3RlYWQuYCxcbiAgICAgICk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGRlZmluaXRpb247XG4gIH1cblxuICBwcm90ZWN0ZWQgYnVpbGRQYXJhbXMoKSB7XG4gICAgY29uc3QgcGFyYW1zID0gc3VwZXIuYnVpbGRQYXJhbXMoKTtcblxuICAgIGlmICh0aGlzLnN3YXBPcGFjaXR5ID09PSB0cnVlIHx8IHRoaXMuc3dhcE9wYWNpdHkgPT09ICd0cnVlJykge1xuICAgICAgcGFyYW1zLmNsYXNzZXMucHVzaCgnZmEtc3dhcC1vcGFjaXR5Jyk7XG4gICAgfVxuICAgIGlmICh0aGlzLnByaW1hcnlPcGFjaXR5ICE9IG51bGwpIHtcbiAgICAgIHBhcmFtcy5zdHlsZXNbJy0tZmEtcHJpbWFyeS1vcGFjaXR5J10gPSB0aGlzLnByaW1hcnlPcGFjaXR5LnRvU3RyaW5nKCk7XG4gICAgfVxuICAgIGlmICh0aGlzLnNlY29uZGFyeU9wYWNpdHkgIT0gbnVsbCkge1xuICAgICAgcGFyYW1zLnN0eWxlc1snLS1mYS1zZWNvbmRhcnktb3BhY2l0eSddID0gdGhpcy5zZWNvbmRhcnlPcGFjaXR5LnRvU3RyaW5nKCk7XG4gICAgfVxuICAgIGlmICh0aGlzLnByaW1hcnlDb2xvciAhPSBudWxsKSB7XG4gICAgICBwYXJhbXMuc3R5bGVzWyctLWZhLXByaW1hcnktY29sb3InXSA9IHRoaXMucHJpbWFyeUNvbG9yO1xuICAgIH1cbiAgICBpZiAodGhpcy5zZWNvbmRhcnlDb2xvciAhPSBudWxsKSB7XG4gICAgICBwYXJhbXMuc3R5bGVzWyctLWZhLXNlY29uZGFyeS1jb2xvciddID0gdGhpcy5zZWNvbmRhcnlDb2xvcjtcbiAgICB9XG5cbiAgICByZXR1cm4gcGFyYW1zO1xuICB9XG59XG4iXX0=