import { Component, HostBinding, Input, Optional } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { findIconDefinition, icon, parse, } from '@fortawesome/fontawesome-svg-core';
import { FaConfig } from '../config';
import { FaIconLibrary } from '../icon-library';
import { faWarnIfIconDefinitionMissing } from '../shared/errors/warn-if-icon-html-missing';
import { faWarnIfIconSpecMissing } from '../shared/errors/warn-if-icon-spec-missing';
import { faClassList } from '../shared/utils/classlist.util';
import { faNormalizeIconSpec } from '../shared/utils/normalize-icon-spec.util';
import { FaStackItemSizeDirective } from '../stack/stack-item-size.directive';
import { FaStackComponent } from '../stack/stack.component';
export class FaIconComponent {
    constructor(sanitizer, config, iconLibrary, stackItem, stack) {
        this.sanitizer = sanitizer;
        this.config = config;
        this.iconLibrary = iconLibrary;
        this.stackItem = stackItem;
        this.classes = [];
        if (stack != null && stackItem == null) {
            console.error('FontAwesome: fa-icon and fa-duotone-icon elements must specify stackItemSize attribute when wrapped into ' +
                'fa-stack. Example: <fa-icon stackItemSize="2x"></fa-icon>.');
        }
    }
    ngOnChanges(changes) {
        if (this.icon == null && this.config.fallbackIcon == null) {
            return faWarnIfIconSpecMissing();
        }
        let iconToBeRendered = null;
        if (this.icon == null) {
            iconToBeRendered = this.config.fallbackIcon;
        }
        else {
            iconToBeRendered = this.icon;
        }
        if (changes) {
            const iconDefinition = this.findIconDefinition(iconToBeRendered);
            if (iconDefinition != null) {
                const params = this.buildParams();
                this.renderIcon(iconDefinition, params);
            }
        }
    }
    /**
     * Programmatically trigger rendering of the icon.
     *
     * This method is useful, when creating {@link FaIconComponent} dynamically or
     * changing its inputs programmatically as in these cases icon won't be
     * re-rendered automatically.
     */
    render() {
        this.ngOnChanges({});
    }
    findIconDefinition(i) {
        const lookup = faNormalizeIconSpec(i, this.config.defaultPrefix);
        if ('icon' in lookup) {
            return lookup;
        }
        const definition = this.iconLibrary.getIconDefinition(lookup.prefix, lookup.iconName);
        if (definition != null) {
            return definition;
        }
        const globalDefinition = findIconDefinition(lookup);
        if (globalDefinition != null) {
            const message = 'Global icon library is deprecated. ' +
                'Consult https://github.com/FortAwesome/angular-fontawesome/blob/master/UPGRADING.md ' +
                'for the migration instructions.';
            if (this.config.globalLibrary === 'unset') {
                console.error('FontAwesome: ' + message);
            }
            else if (!this.config.globalLibrary) {
                throw new Error(message);
            }
            return globalDefinition;
        }
        faWarnIfIconDefinitionMissing(lookup);
        return null;
    }
    buildParams() {
        const classOpts = {
            flip: this.flip,
            spin: this.spin,
            pulse: this.pulse,
            border: this.border,
            inverse: this.inverse,
            size: this.size || null,
            pull: this.pull || null,
            rotate: this.rotate || null,
            fixedWidth: typeof this.fixedWidth === 'boolean' ? this.fixedWidth : this.config.fixedWidth,
            stackItemSize: this.stackItem != null ? this.stackItem.stackItemSize : null,
        };
        const parsedTransform = typeof this.transform === 'string' ? parse.transform(this.transform) : this.transform;
        return {
            title: this.title,
            transform: parsedTransform,
            classes: [...faClassList(classOpts), ...this.classes],
            mask: this.mask != null ? this.findIconDefinition(this.mask) : null,
            styles: this.styles != null ? this.styles : {},
            symbol: this.symbol,
            attributes: {
                role: this.a11yRole,
            },
        };
    }
    renderIcon(definition, params) {
        const renderedIcon = icon(definition, params);
        this.renderedIconHTML = this.sanitizer.bypassSecurityTrustHtml(renderedIcon.html.join('\n'));
    }
}
FaIconComponent.decorators = [
    { type: Component, args: [{
                selector: 'fa-icon',
                template: ``,
                host: {
                    class: 'ng-fa-icon',
                    '[attr.title]': 'title',
                }
            },] }
];
FaIconComponent.ctorParameters = () => [
    { type: DomSanitizer },
    { type: FaConfig },
    { type: FaIconLibrary },
    { type: FaStackItemSizeDirective, decorators: [{ type: Optional }] },
    { type: FaStackComponent, decorators: [{ type: Optional }] }
];
FaIconComponent.propDecorators = {
    icon: [{ type: Input }],
    title: [{ type: Input }],
    spin: [{ type: Input }],
    pulse: [{ type: Input }],
    mask: [{ type: Input }],
    styles: [{ type: Input }],
    flip: [{ type: Input }],
    size: [{ type: Input }],
    pull: [{ type: Input }],
    border: [{ type: Input }],
    inverse: [{ type: Input }],
    symbol: [{ type: Input }],
    rotate: [{ type: Input }],
    fixedWidth: [{ type: Input }],
    classes: [{ type: Input }],
    transform: [{ type: Input }],
    a11yRole: [{ type: Input }],
    renderedIconHTML: [{ type: HostBinding, args: ['innerHTML',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWNvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL2ljb24vaWNvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFhLFFBQVEsRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDbEcsT0FBTyxFQUFFLFlBQVksRUFBWSxNQUFNLDJCQUEyQixDQUFDO0FBQ25FLE9BQU8sRUFFTCxrQkFBa0IsRUFFbEIsSUFBSSxFQUlKLEtBQUssR0FNTixNQUFNLG1DQUFtQyxDQUFDO0FBQzNDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFDckMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ2hELE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQzNGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBRXJGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUM5RSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVU1RCxNQUFNLE9BQU8sZUFBZTtJQWlDMUIsWUFDVSxTQUF1QixFQUN2QixNQUFnQixFQUNoQixXQUEwQixFQUNkLFNBQW1DLEVBQzNDLEtBQXVCO1FBSjNCLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBVTtRQUNoQixnQkFBVyxHQUFYLFdBQVcsQ0FBZTtRQUNkLGNBQVMsR0FBVCxTQUFTLENBQTBCO1FBaEJoRCxZQUFPLEdBQWMsRUFBRSxDQUFDO1FBbUIvQixJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksU0FBUyxJQUFJLElBQUksRUFBRTtZQUN0QyxPQUFPLENBQUMsS0FBSyxDQUNYLDJHQUEyRztnQkFDekcsNERBQTRELENBQy9ELENBQUM7U0FDSDtJQUNILENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksSUFBSSxJQUFJLEVBQUU7WUFDekQsT0FBTyx1QkFBdUIsRUFBRSxDQUFDO1NBQ2xDO1FBRUQsSUFBSSxnQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFDdEMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtZQUNyQixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztTQUM3QzthQUFNO1lBQ0wsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUM5QjtRQUVELElBQUksT0FBTyxFQUFFO1lBQ1gsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDakUsSUFBSSxjQUFjLElBQUksSUFBSSxFQUFFO2dCQUMxQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3pDO1NBQ0Y7SUFDSCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0gsTUFBTTtRQUNKLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVTLGtCQUFrQixDQUFDLENBQTRCO1FBQ3ZELE1BQU0sTUFBTSxHQUFHLG1CQUFtQixDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2pFLElBQUksTUFBTSxJQUFJLE1BQU0sRUFBRTtZQUNwQixPQUFPLE1BQU0sQ0FBQztTQUNmO1FBRUQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0RixJQUFJLFVBQVUsSUFBSSxJQUFJLEVBQUU7WUFDdEIsT0FBTyxVQUFVLENBQUM7U0FDbkI7UUFFRCxNQUFNLGdCQUFnQixHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BELElBQUksZ0JBQWdCLElBQUksSUFBSSxFQUFFO1lBQzVCLE1BQU0sT0FBTyxHQUNYLHFDQUFxQztnQkFDckMsc0ZBQXNGO2dCQUN0RixpQ0FBaUMsQ0FBQztZQUNwQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxLQUFLLE9BQU8sRUFBRTtnQkFDekMsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLENBQUM7YUFDMUM7aUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFO2dCQUNyQyxNQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzFCO1lBRUQsT0FBTyxnQkFBZ0IsQ0FBQztTQUN6QjtRQUVELDZCQUE2QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVTLFdBQVc7UUFDbkIsTUFBTSxTQUFTLEdBQVk7WUFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSTtZQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUk7WUFDM0IsVUFBVSxFQUFFLE9BQU8sSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVTtZQUMzRixhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJO1NBQzVFLENBQUM7UUFFRixNQUFNLGVBQWUsR0FBRyxPQUFPLElBQUksQ0FBQyxTQUFTLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUU5RyxPQUFPO1lBQ0wsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLFNBQVMsRUFBRSxlQUFlO1lBQzFCLE9BQU8sRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUNyRCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDbkUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzlDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRO2FBQ3BCO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUFFTyxVQUFVLENBQUMsVUFBMEIsRUFBRSxNQUFrQjtRQUMvRCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDL0YsQ0FBQzs7O1lBdEpGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsU0FBUztnQkFDbkIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osSUFBSSxFQUFFO29CQUNKLEtBQUssRUFBRSxZQUFZO29CQUNuQixjQUFjLEVBQUUsT0FBTztpQkFDeEI7YUFDRjs7O1lBakNRLFlBQVk7WUFnQlosUUFBUTtZQUNSLGFBQWE7WUFNYix3QkFBd0IsdUJBZ0Q1QixRQUFRO1lBL0NKLGdCQUFnQix1QkFnRHBCLFFBQVE7OzttQkFyQ1YsS0FBSztvQkFPTCxLQUFLO21CQUNMLEtBQUs7b0JBQ0wsS0FBSzttQkFDTCxLQUFLO3FCQUNMLEtBQUs7bUJBQ0wsS0FBSzttQkFDTCxLQUFLO21CQUNMLEtBQUs7cUJBQ0wsS0FBSztzQkFDTCxLQUFLO3FCQUNMLEtBQUs7cUJBQ0wsS0FBSzt5QkFDTCxLQUFLO3NCQUNMLEtBQUs7d0JBQ0wsS0FBSzt1QkFPTCxLQUFLOytCQUVMLFdBQVcsU0FBQyxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBIb3N0QmluZGluZywgSW5wdXQsIE9uQ2hhbmdlcywgT3B0aW9uYWwsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERvbVNhbml0aXplciwgU2FmZUh0bWwgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7XG4gIEZhU3ltYm9sLFxuICBmaW5kSWNvbkRlZmluaXRpb24sXG4gIEZsaXBQcm9wLFxuICBpY29uLFxuICBJY29uRGVmaW5pdGlvbixcbiAgSWNvblBhcmFtcyxcbiAgSWNvblByb3AsXG4gIHBhcnNlLFxuICBQdWxsUHJvcCxcbiAgUm90YXRlUHJvcCxcbiAgU2l6ZVByb3AsXG4gIFN0eWxlcyxcbiAgVHJhbnNmb3JtLFxufSBmcm9tICdAZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtc3ZnLWNvcmUnO1xuaW1wb3J0IHsgRmFDb25maWcgfSBmcm9tICcuLi9jb25maWcnO1xuaW1wb3J0IHsgRmFJY29uTGlicmFyeSB9IGZyb20gJy4uL2ljb24tbGlicmFyeSc7XG5pbXBvcnQgeyBmYVdhcm5JZkljb25EZWZpbml0aW9uTWlzc2luZyB9IGZyb20gJy4uL3NoYXJlZC9lcnJvcnMvd2Fybi1pZi1pY29uLWh0bWwtbWlzc2luZyc7XG5pbXBvcnQgeyBmYVdhcm5JZkljb25TcGVjTWlzc2luZyB9IGZyb20gJy4uL3NoYXJlZC9lcnJvcnMvd2Fybi1pZi1pY29uLXNwZWMtbWlzc2luZyc7XG5pbXBvcnQgeyBGYVByb3BzIH0gZnJvbSAnLi4vc2hhcmVkL21vZGVscy9wcm9wcy5tb2RlbCc7XG5pbXBvcnQgeyBmYUNsYXNzTGlzdCB9IGZyb20gJy4uL3NoYXJlZC91dGlscy9jbGFzc2xpc3QudXRpbCc7XG5pbXBvcnQgeyBmYU5vcm1hbGl6ZUljb25TcGVjIH0gZnJvbSAnLi4vc2hhcmVkL3V0aWxzL25vcm1hbGl6ZS1pY29uLXNwZWMudXRpbCc7XG5pbXBvcnQgeyBGYVN0YWNrSXRlbVNpemVEaXJlY3RpdmUgfSBmcm9tICcuLi9zdGFjay9zdGFjay1pdGVtLXNpemUuZGlyZWN0aXZlJztcbmltcG9ydCB7IEZhU3RhY2tDb21wb25lbnQgfSBmcm9tICcuLi9zdGFjay9zdGFjay5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYS1pY29uJyxcbiAgdGVtcGxhdGU6IGBgLFxuICBob3N0OiB7XG4gICAgY2xhc3M6ICduZy1mYS1pY29uJyxcbiAgICAnW2F0dHIudGl0bGVdJzogJ3RpdGxlJyxcbiAgfSxcbn0pXG5leHBvcnQgY2xhc3MgRmFJY29uQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcbiAgQElucHV0KCkgaWNvbjogSWNvblByb3A7XG5cbiAgLyoqXG4gICAqIFNwZWNpZnkgYSB0aXRsZSBmb3IgdGhlIGljb24uXG4gICAqIFRoaXMgdGV4dCB3aWxsIGJlIGRpc3BsYXllZCBpbiBhIHRvb2x0aXAgb24gaG92ZXIgYW5kIHByZXNlbnRlZCB0byB0aGVcbiAgICogc2NyZWVuIHJlYWRlcnMuXG4gICAqL1xuICBASW5wdXQoKSB0aXRsZT86IHN0cmluZztcbiAgQElucHV0KCkgc3Bpbj86IGJvb2xlYW47XG4gIEBJbnB1dCgpIHB1bHNlPzogYm9vbGVhbjtcbiAgQElucHV0KCkgbWFzaz86IEljb25Qcm9wO1xuICBASW5wdXQoKSBzdHlsZXM/OiBTdHlsZXM7XG4gIEBJbnB1dCgpIGZsaXA/OiBGbGlwUHJvcDtcbiAgQElucHV0KCkgc2l6ZT86IFNpemVQcm9wO1xuICBASW5wdXQoKSBwdWxsPzogUHVsbFByb3A7XG4gIEBJbnB1dCgpIGJvcmRlcj86IGJvb2xlYW47XG4gIEBJbnB1dCgpIGludmVyc2U/OiBib29sZWFuO1xuICBASW5wdXQoKSBzeW1ib2w/OiBGYVN5bWJvbDtcbiAgQElucHV0KCkgcm90YXRlPzogUm90YXRlUHJvcDtcbiAgQElucHV0KCkgZml4ZWRXaWR0aD86IGJvb2xlYW47XG4gIEBJbnB1dCgpIGNsYXNzZXM/OiBzdHJpbmdbXSA9IFtdO1xuICBASW5wdXQoKSB0cmFuc2Zvcm0/OiBzdHJpbmcgfCBUcmFuc2Zvcm07XG5cbiAgLyoqXG4gICAqIFNwZWNpZnkgdGhlIGByb2xlYCBhdHRyaWJ1dGUgZm9yIHRoZSByZW5kZXJlZCA8c3ZnPiBlbGVtZW50LlxuICAgKlxuICAgKiBAZGVmYXVsdCAnaW1nJ1xuICAgKi9cbiAgQElucHV0KCkgYTExeVJvbGU6IHN0cmluZztcblxuICBASG9zdEJpbmRpbmcoJ2lubmVySFRNTCcpIHJlbmRlcmVkSWNvbkhUTUw6IFNhZmVIdG1sO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXIsXG4gICAgcHJpdmF0ZSBjb25maWc6IEZhQ29uZmlnLFxuICAgIHByaXZhdGUgaWNvbkxpYnJhcnk6IEZhSWNvbkxpYnJhcnksXG4gICAgQE9wdGlvbmFsKCkgcHJpdmF0ZSBzdGFja0l0ZW06IEZhU3RhY2tJdGVtU2l6ZURpcmVjdGl2ZSxcbiAgICBAT3B0aW9uYWwoKSBzdGFjazogRmFTdGFja0NvbXBvbmVudCxcbiAgKSB7XG4gICAgaWYgKHN0YWNrICE9IG51bGwgJiYgc3RhY2tJdGVtID09IG51bGwpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoXG4gICAgICAgICdGb250QXdlc29tZTogZmEtaWNvbiBhbmQgZmEtZHVvdG9uZS1pY29uIGVsZW1lbnRzIG11c3Qgc3BlY2lmeSBzdGFja0l0ZW1TaXplIGF0dHJpYnV0ZSB3aGVuIHdyYXBwZWQgaW50byAnICtcbiAgICAgICAgICAnZmEtc3RhY2suIEV4YW1wbGU6IDxmYS1pY29uIHN0YWNrSXRlbVNpemU9XCIyeFwiPjwvZmEtaWNvbj4uJyxcbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgIGlmICh0aGlzLmljb24gPT0gbnVsbCAmJiB0aGlzLmNvbmZpZy5mYWxsYmFja0ljb24gPT0gbnVsbCkge1xuICAgICAgcmV0dXJuIGZhV2FybklmSWNvblNwZWNNaXNzaW5nKCk7XG4gICAgfVxuXG4gICAgbGV0IGljb25Ub0JlUmVuZGVyZWQ6IEljb25Qcm9wID0gbnVsbDtcbiAgICBpZiAodGhpcy5pY29uID09IG51bGwpIHtcbiAgICAgIGljb25Ub0JlUmVuZGVyZWQgPSB0aGlzLmNvbmZpZy5mYWxsYmFja0ljb247XG4gICAgfSBlbHNlIHtcbiAgICAgIGljb25Ub0JlUmVuZGVyZWQgPSB0aGlzLmljb247XG4gICAgfVxuXG4gICAgaWYgKGNoYW5nZXMpIHtcbiAgICAgIGNvbnN0IGljb25EZWZpbml0aW9uID0gdGhpcy5maW5kSWNvbkRlZmluaXRpb24oaWNvblRvQmVSZW5kZXJlZCk7XG4gICAgICBpZiAoaWNvbkRlZmluaXRpb24gIT0gbnVsbCkge1xuICAgICAgICBjb25zdCBwYXJhbXMgPSB0aGlzLmJ1aWxkUGFyYW1zKCk7XG4gICAgICAgIHRoaXMucmVuZGVySWNvbihpY29uRGVmaW5pdGlvbiwgcGFyYW1zKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUHJvZ3JhbW1hdGljYWxseSB0cmlnZ2VyIHJlbmRlcmluZyBvZiB0aGUgaWNvbi5cbiAgICpcbiAgICogVGhpcyBtZXRob2QgaXMgdXNlZnVsLCB3aGVuIGNyZWF0aW5nIHtAbGluayBGYUljb25Db21wb25lbnR9IGR5bmFtaWNhbGx5IG9yXG4gICAqIGNoYW5naW5nIGl0cyBpbnB1dHMgcHJvZ3JhbW1hdGljYWxseSBhcyBpbiB0aGVzZSBjYXNlcyBpY29uIHdvbid0IGJlXG4gICAqIHJlLXJlbmRlcmVkIGF1dG9tYXRpY2FsbHkuXG4gICAqL1xuICByZW5kZXIoKSB7XG4gICAgdGhpcy5uZ09uQ2hhbmdlcyh7fSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZmluZEljb25EZWZpbml0aW9uKGk6IEljb25Qcm9wIHwgSWNvbkRlZmluaXRpb24pOiBJY29uRGVmaW5pdGlvbiB8IG51bGwge1xuICAgIGNvbnN0IGxvb2t1cCA9IGZhTm9ybWFsaXplSWNvblNwZWMoaSwgdGhpcy5jb25maWcuZGVmYXVsdFByZWZpeCk7XG4gICAgaWYgKCdpY29uJyBpbiBsb29rdXApIHtcbiAgICAgIHJldHVybiBsb29rdXA7XG4gICAgfVxuXG4gICAgY29uc3QgZGVmaW5pdGlvbiA9IHRoaXMuaWNvbkxpYnJhcnkuZ2V0SWNvbkRlZmluaXRpb24obG9va3VwLnByZWZpeCwgbG9va3VwLmljb25OYW1lKTtcbiAgICBpZiAoZGVmaW5pdGlvbiAhPSBudWxsKSB7XG4gICAgICByZXR1cm4gZGVmaW5pdGlvbjtcbiAgICB9XG5cbiAgICBjb25zdCBnbG9iYWxEZWZpbml0aW9uID0gZmluZEljb25EZWZpbml0aW9uKGxvb2t1cCk7XG4gICAgaWYgKGdsb2JhbERlZmluaXRpb24gIT0gbnVsbCkge1xuICAgICAgY29uc3QgbWVzc2FnZSA9XG4gICAgICAgICdHbG9iYWwgaWNvbiBsaWJyYXJ5IGlzIGRlcHJlY2F0ZWQuICcgK1xuICAgICAgICAnQ29uc3VsdCBodHRwczovL2dpdGh1Yi5jb20vRm9ydEF3ZXNvbWUvYW5ndWxhci1mb250YXdlc29tZS9ibG9iL21hc3Rlci9VUEdSQURJTkcubWQgJyArXG4gICAgICAgICdmb3IgdGhlIG1pZ3JhdGlvbiBpbnN0cnVjdGlvbnMuJztcbiAgICAgIGlmICh0aGlzLmNvbmZpZy5nbG9iYWxMaWJyYXJ5ID09PSAndW5zZXQnKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ZvbnRBd2Vzb21lOiAnICsgbWVzc2FnZSk7XG4gICAgICB9IGVsc2UgaWYgKCF0aGlzLmNvbmZpZy5nbG9iYWxMaWJyYXJ5KSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGdsb2JhbERlZmluaXRpb247XG4gICAgfVxuXG4gICAgZmFXYXJuSWZJY29uRGVmaW5pdGlvbk1pc3NpbmcobG9va3VwKTtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByb3RlY3RlZCBidWlsZFBhcmFtcygpIHtcbiAgICBjb25zdCBjbGFzc09wdHM6IEZhUHJvcHMgPSB7XG4gICAgICBmbGlwOiB0aGlzLmZsaXAsXG4gICAgICBzcGluOiB0aGlzLnNwaW4sXG4gICAgICBwdWxzZTogdGhpcy5wdWxzZSxcbiAgICAgIGJvcmRlcjogdGhpcy5ib3JkZXIsXG4gICAgICBpbnZlcnNlOiB0aGlzLmludmVyc2UsXG4gICAgICBzaXplOiB0aGlzLnNpemUgfHwgbnVsbCxcbiAgICAgIHB1bGw6IHRoaXMucHVsbCB8fCBudWxsLFxuICAgICAgcm90YXRlOiB0aGlzLnJvdGF0ZSB8fCBudWxsLFxuICAgICAgZml4ZWRXaWR0aDogdHlwZW9mIHRoaXMuZml4ZWRXaWR0aCA9PT0gJ2Jvb2xlYW4nID8gdGhpcy5maXhlZFdpZHRoIDogdGhpcy5jb25maWcuZml4ZWRXaWR0aCxcbiAgICAgIHN0YWNrSXRlbVNpemU6IHRoaXMuc3RhY2tJdGVtICE9IG51bGwgPyB0aGlzLnN0YWNrSXRlbS5zdGFja0l0ZW1TaXplIDogbnVsbCxcbiAgICB9O1xuXG4gICAgY29uc3QgcGFyc2VkVHJhbnNmb3JtID0gdHlwZW9mIHRoaXMudHJhbnNmb3JtID09PSAnc3RyaW5nJyA/IHBhcnNlLnRyYW5zZm9ybSh0aGlzLnRyYW5zZm9ybSkgOiB0aGlzLnRyYW5zZm9ybTtcblxuICAgIHJldHVybiB7XG4gICAgICB0aXRsZTogdGhpcy50aXRsZSxcbiAgICAgIHRyYW5zZm9ybTogcGFyc2VkVHJhbnNmb3JtLFxuICAgICAgY2xhc3NlczogWy4uLmZhQ2xhc3NMaXN0KGNsYXNzT3B0cyksIC4uLnRoaXMuY2xhc3Nlc10sXG4gICAgICBtYXNrOiB0aGlzLm1hc2sgIT0gbnVsbCA/IHRoaXMuZmluZEljb25EZWZpbml0aW9uKHRoaXMubWFzaykgOiBudWxsLFxuICAgICAgc3R5bGVzOiB0aGlzLnN0eWxlcyAhPSBudWxsID8gdGhpcy5zdHlsZXMgOiB7fSxcbiAgICAgIHN5bWJvbDogdGhpcy5zeW1ib2wsXG4gICAgICBhdHRyaWJ1dGVzOiB7XG4gICAgICAgIHJvbGU6IHRoaXMuYTExeVJvbGUsXG4gICAgICB9LFxuICAgIH07XG4gIH1cblxuICBwcml2YXRlIHJlbmRlckljb24oZGVmaW5pdGlvbjogSWNvbkRlZmluaXRpb24sIHBhcmFtczogSWNvblBhcmFtcykge1xuICAgIGNvbnN0IHJlbmRlcmVkSWNvbiA9IGljb24oZGVmaW5pdGlvbiwgcGFyYW1zKTtcbiAgICB0aGlzLnJlbmRlcmVkSWNvbkhUTUwgPSB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0SHRtbChyZW5kZXJlZEljb24uaHRtbC5qb2luKCdcXG4nKSk7XG4gIH1cbn1cbiJdfQ==