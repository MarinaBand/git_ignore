import { Directive, Input } from '@angular/core';
export class FaStackItemSizeDirective {
    constructor() {
        /**
         * Specify whether icon inside {@link FaStackComponent} should be rendered in
         * regular size (1x) or as a larger icon (2x).
         */
        this.stackItemSize = '1x';
    }
    ngOnChanges(changes) {
        if ('size' in changes) {
            throw new Error('fa-icon is not allowed to customize size when used inside fa-stack. ' +
                'Set size on the enclosing fa-stack instead: <fa-stack size="4x">...</fa-stack>.');
        }
    }
}
FaStackItemSizeDirective.decorators = [
    { type: Directive, args: [{
                // eslint-disable-next-line @angular-eslint/directive-selector
                selector: 'fa-icon[stackItemSize],fa-duotone-icon[stackItemSize]',
            },] }
];
FaStackItemSizeDirective.propDecorators = {
    stackItemSize: [{ type: Input }],
    size: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhY2staXRlbS1zaXplLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvc3RhY2svc3RhY2staXRlbS1zaXplLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFRM0UsTUFBTSxPQUFPLHdCQUF3QjtJQUpyQztRQUtFOzs7V0FHRztRQUNNLGtCQUFhLEdBQWdCLElBQUksQ0FBQztJQWU3QyxDQUFDO0lBUkMsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksTUFBTSxJQUFJLE9BQU8sRUFBRTtZQUNyQixNQUFNLElBQUksS0FBSyxDQUNiLHNFQUFzRTtnQkFDcEUsaUZBQWlGLENBQ3BGLENBQUM7U0FDSDtJQUNILENBQUM7OztZQXZCRixTQUFTLFNBQUM7Z0JBQ1QsOERBQThEO2dCQUM5RCxRQUFRLEVBQUUsdURBQXVEO2FBQ2xFOzs7NEJBTUUsS0FBSzttQkFLTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTaXplUHJvcCB9IGZyb20gJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1zdmctY29yZSc7XG5pbXBvcnQgeyBGYVN0YWNrQ29tcG9uZW50IH0gZnJvbSAnLi9zdGFjay5jb21wb25lbnQnO1xuXG5ARGlyZWN0aXZlKHtcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIEBhbmd1bGFyLWVzbGludC9kaXJlY3RpdmUtc2VsZWN0b3JcbiAgc2VsZWN0b3I6ICdmYS1pY29uW3N0YWNrSXRlbVNpemVdLGZhLWR1b3RvbmUtaWNvbltzdGFja0l0ZW1TaXplXScsXG59KVxuZXhwb3J0IGNsYXNzIEZhU3RhY2tJdGVtU2l6ZURpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG4gIC8qKlxuICAgKiBTcGVjaWZ5IHdoZXRoZXIgaWNvbiBpbnNpZGUge0BsaW5rIEZhU3RhY2tDb21wb25lbnR9IHNob3VsZCBiZSByZW5kZXJlZCBpblxuICAgKiByZWd1bGFyIHNpemUgKDF4KSBvciBhcyBhIGxhcmdlciBpY29uICgyeCkuXG4gICAqL1xuICBASW5wdXQoKSBzdGFja0l0ZW1TaXplOiAnMXgnIHwgJzJ4JyA9ICcxeCc7XG5cbiAgLyoqXG4gICAqIEBpbnRlcm5hbFxuICAgKi9cbiAgQElucHV0KCkgc2l6ZT86IFNpemVQcm9wO1xuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICBpZiAoJ3NpemUnIGluIGNoYW5nZXMpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgJ2ZhLWljb24gaXMgbm90IGFsbG93ZWQgdG8gY3VzdG9taXplIHNpemUgd2hlbiB1c2VkIGluc2lkZSBmYS1zdGFjay4gJyArXG4gICAgICAgICAgJ1NldCBzaXplIG9uIHRoZSBlbmNsb3NpbmcgZmEtc3RhY2sgaW5zdGVhZDogPGZhLXN0YWNrIHNpemU9XCI0eFwiPi4uLjwvZmEtc3RhY2s+LicsXG4gICAgICApO1xuICAgIH1cbiAgfVxufVxuIl19