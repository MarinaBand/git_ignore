import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class FaConfig {
    constructor() {
        /**
         * Default prefix to use, when one is not provided with the icon name.
         *
         * @default 'fas'
         */
        this.defaultPrefix = 'fas';
        /**
         * Provides a fallback icon to use whilst main icon is being loaded asynchronously.
         * When value is null, then fa-icon component will throw an error if icon input is missing.
         * When value is not null, then the provided icon will be used as a fallback icon if icon input is missing.
         *
         * @default null
         */
        this.fallbackIcon = null;
        /**
         * Whether components should lookup icon definitions in the global icon
         * library (the one available from
         * `import { library } from '@fortawesome/fontawesome-svg-core')`.
         *
         * See https://github.com/FortAwesome/angular-fontawesome/blob/master/docs/usage/icon-library.md
         * for detailed description of library modes.
         *
         * - 'unset' - Components should lookup icon definitions in the global library
         * and emit warning if they find a definition there. This option is a default
         * to assist existing applications with a migration. Applications are expected
         * to switch to using {@link FaIconLibrary}.
         * - true - Components should lookup icon definitions in the global library.
         * Note that global icon library is deprecated and support for it will be
         * removed. This option can be used to temporarily suppress warnings.
         * - false - Components should not lookup icon definitions in the global
         * library. Library will throw an error if missing icon is found in the global
         * library.
         *
         * @deprecated This option is deprecated since 0.5.0. In 0.6.0 default will
         * be changed to false. In 0.8.0 the option will be removed together with the
         * support for the global icon library.
         *
         * @default false
         */
        this.globalLibrary = false;
    }
}
FaConfig.ɵprov = i0.ɵɵdefineInjectable({ factory: function FaConfig_Factory() { return new FaConfig(); }, token: FaConfig, providedIn: "root" });
FaConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLM0MsTUFBTSxPQUFPLFFBQVE7SUFEckI7UUFFRTs7OztXQUlHO1FBQ0gsa0JBQWEsR0FBZSxLQUFLLENBQUM7UUFFbEM7Ozs7OztXQU1HO1FBQ0gsaUJBQVksR0FBbUIsSUFBSSxDQUFDO1FBVXBDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7V0F3Qkc7UUFDSCxrQkFBYSxHQUFzQixLQUFLLENBQUM7S0FDMUM7Ozs7WUFwREEsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEljb25EZWZpbml0aW9uLCBJY29uUHJlZml4IH0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWNvbW1vbi10eXBlcyc7XG5pbXBvcnQgeyBGYUljb25MaWJyYXJ5IH0gZnJvbSAnLi9pY29uLWxpYnJhcnknO1xuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuZXhwb3J0IGNsYXNzIEZhQ29uZmlnIHtcbiAgLyoqXG4gICAqIERlZmF1bHQgcHJlZml4IHRvIHVzZSwgd2hlbiBvbmUgaXMgbm90IHByb3ZpZGVkIHdpdGggdGhlIGljb24gbmFtZS5cbiAgICpcbiAgICogQGRlZmF1bHQgJ2ZhcydcbiAgICovXG4gIGRlZmF1bHRQcmVmaXg6IEljb25QcmVmaXggPSAnZmFzJztcblxuICAvKipcbiAgICogUHJvdmlkZXMgYSBmYWxsYmFjayBpY29uIHRvIHVzZSB3aGlsc3QgbWFpbiBpY29uIGlzIGJlaW5nIGxvYWRlZCBhc3luY2hyb25vdXNseS5cbiAgICogV2hlbiB2YWx1ZSBpcyBudWxsLCB0aGVuIGZhLWljb24gY29tcG9uZW50IHdpbGwgdGhyb3cgYW4gZXJyb3IgaWYgaWNvbiBpbnB1dCBpcyBtaXNzaW5nLlxuICAgKiBXaGVuIHZhbHVlIGlzIG5vdCBudWxsLCB0aGVuIHRoZSBwcm92aWRlZCBpY29uIHdpbGwgYmUgdXNlZCBhcyBhIGZhbGxiYWNrIGljb24gaWYgaWNvbiBpbnB1dCBpcyBtaXNzaW5nLlxuICAgKlxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqL1xuICBmYWxsYmFja0ljb246IEljb25EZWZpbml0aW9uID0gbnVsbDtcblxuICAvKipcbiAgICogU2V0IGljb25zIHRvIHRoZSBzYW1lIGZpeGVkIHdpZHRoLlxuICAgKlxuICAgKiBAc2VlIHtAbGluazogaHR0cHM6Ly9mb250YXdlc29tZS5jb20vaG93LXRvLXVzZS9vbi10aGUtd2ViL3N0eWxpbmcvZml4ZWQtd2lkdGgtaWNvbnN9XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBmaXhlZFdpZHRoPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogV2hldGhlciBjb21wb25lbnRzIHNob3VsZCBsb29rdXAgaWNvbiBkZWZpbml0aW9ucyBpbiB0aGUgZ2xvYmFsIGljb25cbiAgICogbGlicmFyeSAodGhlIG9uZSBhdmFpbGFibGUgZnJvbVxuICAgKiBgaW1wb3J0IHsgbGlicmFyeSB9IGZyb20gJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1zdmctY29yZScpYC5cbiAgICpcbiAgICogU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9Gb3J0QXdlc29tZS9hbmd1bGFyLWZvbnRhd2Vzb21lL2Jsb2IvbWFzdGVyL2RvY3MvdXNhZ2UvaWNvbi1saWJyYXJ5Lm1kXG4gICAqIGZvciBkZXRhaWxlZCBkZXNjcmlwdGlvbiBvZiBsaWJyYXJ5IG1vZGVzLlxuICAgKlxuICAgKiAtICd1bnNldCcgLSBDb21wb25lbnRzIHNob3VsZCBsb29rdXAgaWNvbiBkZWZpbml0aW9ucyBpbiB0aGUgZ2xvYmFsIGxpYnJhcnlcbiAgICogYW5kIGVtaXQgd2FybmluZyBpZiB0aGV5IGZpbmQgYSBkZWZpbml0aW9uIHRoZXJlLiBUaGlzIG9wdGlvbiBpcyBhIGRlZmF1bHRcbiAgICogdG8gYXNzaXN0IGV4aXN0aW5nIGFwcGxpY2F0aW9ucyB3aXRoIGEgbWlncmF0aW9uLiBBcHBsaWNhdGlvbnMgYXJlIGV4cGVjdGVkXG4gICAqIHRvIHN3aXRjaCB0byB1c2luZyB7QGxpbmsgRmFJY29uTGlicmFyeX0uXG4gICAqIC0gdHJ1ZSAtIENvbXBvbmVudHMgc2hvdWxkIGxvb2t1cCBpY29uIGRlZmluaXRpb25zIGluIHRoZSBnbG9iYWwgbGlicmFyeS5cbiAgICogTm90ZSB0aGF0IGdsb2JhbCBpY29uIGxpYnJhcnkgaXMgZGVwcmVjYXRlZCBhbmQgc3VwcG9ydCBmb3IgaXQgd2lsbCBiZVxuICAgKiByZW1vdmVkLiBUaGlzIG9wdGlvbiBjYW4gYmUgdXNlZCB0byB0ZW1wb3JhcmlseSBzdXBwcmVzcyB3YXJuaW5ncy5cbiAgICogLSBmYWxzZSAtIENvbXBvbmVudHMgc2hvdWxkIG5vdCBsb29rdXAgaWNvbiBkZWZpbml0aW9ucyBpbiB0aGUgZ2xvYmFsXG4gICAqIGxpYnJhcnkuIExpYnJhcnkgd2lsbCB0aHJvdyBhbiBlcnJvciBpZiBtaXNzaW5nIGljb24gaXMgZm91bmQgaW4gdGhlIGdsb2JhbFxuICAgKiBsaWJyYXJ5LlxuICAgKlxuICAgKiBAZGVwcmVjYXRlZCBUaGlzIG9wdGlvbiBpcyBkZXByZWNhdGVkIHNpbmNlIDAuNS4wLiBJbiAwLjYuMCBkZWZhdWx0IHdpbGxcbiAgICogYmUgY2hhbmdlZCB0byBmYWxzZS4gSW4gMC44LjAgdGhlIG9wdGlvbiB3aWxsIGJlIHJlbW92ZWQgdG9nZXRoZXIgd2l0aCB0aGVcbiAgICogc3VwcG9ydCBmb3IgdGhlIGdsb2JhbCBpY29uIGxpYnJhcnkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBnbG9iYWxMaWJyYXJ5OiBib29sZWFuIHwgJ3Vuc2V0JyA9IGZhbHNlO1xufVxuIl19