import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class FaIconLibrary {
    constructor() {
        this.definitions = {};
    }
    addIcons(...icons) {
        for (const icon of icons) {
            if (!(icon.prefix in this.definitions)) {
                this.definitions[icon.prefix] = {};
            }
            this.definitions[icon.prefix][icon.iconName] = icon;
        }
    }
    addIconPacks(...packs) {
        for (const pack of packs) {
            const icons = Object.keys(pack).map((key) => pack[key]);
            this.addIcons(...icons);
        }
    }
    getIconDefinition(prefix, name) {
        if (prefix in this.definitions && name in this.definitions[prefix]) {
            return this.definitions[prefix][name];
        }
        return null;
    }
}
FaIconLibrary.ɵprov = i0.ɵɵdefineInjectable({ factory: function FaIconLibrary_Factory() { return new FaIconLibrary(); }, token: FaIconLibrary, providedIn: "root" });
FaIconLibrary.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWNvbi1saWJyYXJ5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9pY29uLWxpYnJhcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFVM0MsTUFBTSxPQUFPLGFBQWE7SUFEMUI7UUFFVSxnQkFBVyxHQUE2RCxFQUFFLENBQUM7S0F3QnBGO0lBdEJDLFFBQVEsQ0FBQyxHQUFHLEtBQXVCO1FBQ2pDLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDcEM7WUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO1NBQ3JEO0lBQ0gsQ0FBQztJQUVELFlBQVksQ0FBQyxHQUFHLEtBQWlCO1FBQy9CLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO1lBQ3hCLE1BQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsTUFBa0IsRUFBRSxJQUFjO1FBQ2xELElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDbEUsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZDO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O1lBekJGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJY29uRGVmaW5pdGlvbiwgSWNvbk5hbWUsIEljb25QYWNrLCBJY29uUHJlZml4IH0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLXN2Zy1jb3JlJztcblxuZXhwb3J0IGludGVyZmFjZSBGYUljb25MaWJyYXJ5SW50ZXJmYWNlIHtcbiAgYWRkSWNvbnMoLi4uaWNvbnM6IEljb25EZWZpbml0aW9uW10pOiB2b2lkO1xuICBhZGRJY29uUGFja3MoLi4ucGFja3M6IEljb25QYWNrW10pOiB2b2lkO1xuICBnZXRJY29uRGVmaW5pdGlvbihwcmVmaXg6IEljb25QcmVmaXgsIG5hbWU6IEljb25OYW1lKTogSWNvbkRlZmluaXRpb24gfCBudWxsO1xufVxuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuZXhwb3J0IGNsYXNzIEZhSWNvbkxpYnJhcnkgaW1wbGVtZW50cyBGYUljb25MaWJyYXJ5SW50ZXJmYWNlIHtcbiAgcHJpdmF0ZSBkZWZpbml0aW9uczogeyBbcHJlZml4OiBzdHJpbmddOiB7IFtuYW1lOiBzdHJpbmddOiBJY29uRGVmaW5pdGlvbiB9IH0gPSB7fTtcblxuICBhZGRJY29ucyguLi5pY29uczogSWNvbkRlZmluaXRpb25bXSkge1xuICAgIGZvciAoY29uc3QgaWNvbiBvZiBpY29ucykge1xuICAgICAgaWYgKCEoaWNvbi5wcmVmaXggaW4gdGhpcy5kZWZpbml0aW9ucykpIHtcbiAgICAgICAgdGhpcy5kZWZpbml0aW9uc1tpY29uLnByZWZpeF0gPSB7fTtcbiAgICAgIH1cbiAgICAgIHRoaXMuZGVmaW5pdGlvbnNbaWNvbi5wcmVmaXhdW2ljb24uaWNvbk5hbWVdID0gaWNvbjtcbiAgICB9XG4gIH1cblxuICBhZGRJY29uUGFja3MoLi4ucGFja3M6IEljb25QYWNrW10pIHtcbiAgICBmb3IgKGNvbnN0IHBhY2sgb2YgcGFja3MpIHtcbiAgICAgIGNvbnN0IGljb25zID0gT2JqZWN0LmtleXMocGFjaykubWFwKChrZXkpID0+IHBhY2tba2V5XSk7XG4gICAgICB0aGlzLmFkZEljb25zKC4uLmljb25zKTtcbiAgICB9XG4gIH1cblxuICBnZXRJY29uRGVmaW5pdGlvbihwcmVmaXg6IEljb25QcmVmaXgsIG5hbWU6IEljb25OYW1lKTogSWNvbkRlZmluaXRpb24gfCBudWxsIHtcbiAgICBpZiAocHJlZml4IGluIHRoaXMuZGVmaW5pdGlvbnMgJiYgbmFtZSBpbiB0aGlzLmRlZmluaXRpb25zW3ByZWZpeF0pIHtcbiAgICAgIHJldHVybiB0aGlzLmRlZmluaXRpb25zW3ByZWZpeF1bbmFtZV07XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG59XG4iXX0=