import { NgModule } from '@angular/core';
import { FaDuotoneIconComponent } from './icon/duotone-icon.component';
import { FaIconComponent } from './icon/icon.component';
import { FaLayersCounterComponent } from './layers/layers-counter.component';
import { FaLayersTextComponent } from './layers/layers-text.component';
import { FaLayersComponent } from './layers/layers.component';
import { FaStackItemSizeDirective } from './stack/stack-item-size.directive';
import { FaStackComponent } from './stack/stack.component';
export class FontAwesomeModule {
}
FontAwesomeModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    FaIconComponent,
                    FaDuotoneIconComponent,
                    FaLayersComponent,
                    FaLayersTextComponent,
                    FaLayersCounterComponent,
                    FaStackComponent,
                    FaStackItemSizeDirective,
                ],
                exports: [
                    FaIconComponent,
                    FaDuotoneIconComponent,
                    FaLayersComponent,
                    FaLayersTextComponent,
                    FaLayersCounterComponent,
                    FaStackComponent,
                    FaStackItemSizeDirective,
                ],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9udGF3ZXNvbWUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9mb250YXdlc29tZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUN2RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDN0UsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDOUQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDN0UsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFzQjNELE1BQU0sT0FBTyxpQkFBaUI7OztZQXBCN0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixlQUFlO29CQUNmLHNCQUFzQjtvQkFDdEIsaUJBQWlCO29CQUNqQixxQkFBcUI7b0JBQ3JCLHdCQUF3QjtvQkFDeEIsZ0JBQWdCO29CQUNoQix3QkFBd0I7aUJBQ3pCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxlQUFlO29CQUNmLHNCQUFzQjtvQkFDdEIsaUJBQWlCO29CQUNqQixxQkFBcUI7b0JBQ3JCLHdCQUF3QjtvQkFDeEIsZ0JBQWdCO29CQUNoQix3QkFBd0I7aUJBQ3pCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRmFEdW90b25lSWNvbkNvbXBvbmVudCB9IGZyb20gJy4vaWNvbi9kdW90b25lLWljb24uY29tcG9uZW50JztcbmltcG9ydCB7IEZhSWNvbkNvbXBvbmVudCB9IGZyb20gJy4vaWNvbi9pY29uLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGYUxheWVyc0NvdW50ZXJDb21wb25lbnQgfSBmcm9tICcuL2xheWVycy9sYXllcnMtY291bnRlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmFMYXllcnNUZXh0Q29tcG9uZW50IH0gZnJvbSAnLi9sYXllcnMvbGF5ZXJzLXRleHQuY29tcG9uZW50JztcbmltcG9ydCB7IEZhTGF5ZXJzQ29tcG9uZW50IH0gZnJvbSAnLi9sYXllcnMvbGF5ZXJzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGYVN0YWNrSXRlbVNpemVEaXJlY3RpdmUgfSBmcm9tICcuL3N0YWNrL3N0YWNrLWl0ZW0tc2l6ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgRmFTdGFja0NvbXBvbmVudCB9IGZyb20gJy4vc3RhY2svc3RhY2suY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRmFJY29uQ29tcG9uZW50LFxuICAgIEZhRHVvdG9uZUljb25Db21wb25lbnQsXG4gICAgRmFMYXllcnNDb21wb25lbnQsXG4gICAgRmFMYXllcnNUZXh0Q29tcG9uZW50LFxuICAgIEZhTGF5ZXJzQ291bnRlckNvbXBvbmVudCxcbiAgICBGYVN0YWNrQ29tcG9uZW50LFxuICAgIEZhU3RhY2tJdGVtU2l6ZURpcmVjdGl2ZSxcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEZhSWNvbkNvbXBvbmVudCxcbiAgICBGYUR1b3RvbmVJY29uQ29tcG9uZW50LFxuICAgIEZhTGF5ZXJzQ29tcG9uZW50LFxuICAgIEZhTGF5ZXJzVGV4dENvbXBvbmVudCxcbiAgICBGYUxheWVyc0NvdW50ZXJDb21wb25lbnQsXG4gICAgRmFTdGFja0NvbXBvbmVudCxcbiAgICBGYVN0YWNrSXRlbVNpemVEaXJlY3RpdmUsXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEZvbnRBd2Vzb21lTW9kdWxlIHt9XG4iXX0=