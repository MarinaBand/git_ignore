import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export const dummyIcon = {
    prefix: 'fad',
    iconName: 'dummy',
    icon: [512, 512, [], 'f030', 'M50 50 H462 V462 H50 Z'],
};
export class MockFaIconLibrary {
    addIcons() {
        throw new Error('Attempt to add an icon to the MockFaIconLibrary.');
    }
    addIconPacks() {
        throw new Error('Attempt to add an icon pack to the MockFaIconLibrary.');
    }
    getIconDefinition(prefix, name) {
        return dummyIcon;
    }
}
MockFaIconLibrary.ɵprov = i0.ɵɵdefineInjectable({ factory: function MockFaIconLibrary_Factory() { return new MockFaIconLibrary(); }, token: MockFaIconLibrary, providedIn: "root" });
MockFaIconLibrary.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9jay1pY29uLWxpYnJhcnkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Rlc3Rpbmcvc3JjL2ljb24vbW9jay1pY29uLWxpYnJhcnkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUkzQyxNQUFNLENBQUMsTUFBTSxTQUFTLEdBQW1CO0lBQ3ZDLE1BQU0sRUFBRSxLQUFLO0lBQ2IsUUFBUSxFQUFFLE9BQW1CO0lBQzdCLElBQUksRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSx3QkFBd0IsQ0FBQztDQUN2RCxDQUFDO0FBS0YsTUFBTSxPQUFPLGlCQUFpQjtJQUM1QixRQUFRO1FBQ04sTUFBTSxJQUFJLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRCxZQUFZO1FBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyx1REFBdUQsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxNQUFrQixFQUFFLElBQWM7UUFDbEQsT0FBTyxTQUFTLENBQUM7SUFDbkIsQ0FBQzs7OztZQWRGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZhSWNvbkxpYnJhcnlJbnRlcmZhY2UgfSBmcm9tICdAZm9ydGF3ZXNvbWUvYW5ndWxhci1mb250YXdlc29tZSc7XG5pbXBvcnQgeyBJY29uRGVmaW5pdGlvbiwgSWNvbk5hbWUsIEljb25QcmVmaXggfSBmcm9tICdAZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtc3ZnLWNvcmUnO1xuXG5leHBvcnQgY29uc3QgZHVtbXlJY29uOiBJY29uRGVmaW5pdGlvbiA9IHtcbiAgcHJlZml4OiAnZmFkJyxcbiAgaWNvbk5hbWU6ICdkdW1teScgYXMgSWNvbk5hbWUsXG4gIGljb246IFs1MTIsIDUxMiwgW10sICdmMDMwJywgJ001MCA1MCBINDYyIFY0NjIgSDUwIFonXSxcbn07XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnLFxufSlcbmV4cG9ydCBjbGFzcyBNb2NrRmFJY29uTGlicmFyeSBpbXBsZW1lbnRzIEZhSWNvbkxpYnJhcnlJbnRlcmZhY2Uge1xuICBhZGRJY29ucygpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0dGVtcHQgdG8gYWRkIGFuIGljb24gdG8gdGhlIE1vY2tGYUljb25MaWJyYXJ5LicpO1xuICB9XG5cbiAgYWRkSWNvblBhY2tzKCkge1xuICAgIHRocm93IG5ldyBFcnJvcignQXR0ZW1wdCB0byBhZGQgYW4gaWNvbiBwYWNrIHRvIHRoZSBNb2NrRmFJY29uTGlicmFyeS4nKTtcbiAgfVxuXG4gIGdldEljb25EZWZpbml0aW9uKHByZWZpeDogSWNvblByZWZpeCwgbmFtZTogSWNvbk5hbWUpOiBJY29uRGVmaW5pdGlvbiB7XG4gICAgcmV0dXJuIGR1bW15SWNvbjtcbiAgfVxufVxuIl19