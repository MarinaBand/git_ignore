import { ElementRef, OnChanges, OnInit, Renderer2, SimpleChanges } from '@angular/core';
import { SizeProp } from '@fortawesome/fontawesome-svg-core';
import { FaConfig } from '../config';
/**
 * Fontawesome layers.
 */
import * as ɵngcc0 from '@angular/core';
export declare class FaLayersComponent implements OnInit, OnChanges {
    private renderer;
    private elementRef;
    private config;
    size?: SizeProp;
    fixedWidth?: boolean;
    constructor(renderer: Renderer2, elementRef: ElementRef, config: FaConfig);
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaLayersComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDeclaration<FaLayersComponent, "fa-layers", never, { "fixedWidth": "fixedWidth"; "size": "size"; }, {}, never, ["*"]>;
}

//# sourceMappingURL=layers.component.d.ts.map