import { OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { CounterParams, Styles } from '@fortawesome/fontawesome-svg-core';
import { FaLayersComponent } from './layers.component';
import * as ɵngcc0 from '@angular/core';
export declare class FaLayersCounterComponent implements OnChanges {
    private parent;
    private sanitizer;
    content: string;
    title?: string;
    styles?: Styles;
    classes?: string[];
    renderedHTML: SafeHtml;
    constructor(parent: FaLayersComponent, sanitizer: DomSanitizer);
    ngOnChanges(changes: SimpleChanges): void;
    protected buildParams(): CounterParams;
    private updateContent;
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaLayersCounterComponent, [{ optional: true; }, null]>;
    static ɵcmp: ɵngcc0.ɵɵComponentDeclaration<FaLayersCounterComponent, "fa-layers-counter", never, { "classes": "classes"; "content": "content"; "title": "title"; "styles": "styles"; }, {}, never, never>;
}

//# sourceMappingURL=layers-counter.component.d.ts.map