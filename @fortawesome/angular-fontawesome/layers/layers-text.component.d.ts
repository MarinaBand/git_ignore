import { OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { FlipProp, PullProp, RotateProp, SizeProp, Styles, TextParams, Transform } from '@fortawesome/fontawesome-svg-core';
import { FaLayersComponent } from './layers.component';
import * as ɵngcc0 from '@angular/core';
export declare class FaLayersTextComponent implements OnChanges {
    private parent;
    private sanitizer;
    content: string;
    title?: string;
    styles?: Styles;
    classes?: string[];
    spin?: boolean;
    pulse?: boolean;
    flip?: FlipProp;
    size?: SizeProp;
    pull?: PullProp;
    border?: boolean;
    inverse?: boolean;
    rotate?: RotateProp;
    fixedWidth?: boolean;
    transform?: string | Transform;
    renderedHTML: SafeHtml;
    constructor(parent: FaLayersComponent, sanitizer: DomSanitizer);
    ngOnChanges(changes: SimpleChanges): void;
    /**
     * Updating params by component props.
     */
    protected buildParams(): TextParams;
    private updateContent;
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<FaLayersTextComponent, [{ optional: true; }, null]>;
    static ɵcmp: ɵngcc0.ɵɵComponentDeclaration<FaLayersTextComponent, "fa-layers-text", never, { "classes": "classes"; "content": "content"; "title": "title"; "styles": "styles"; "spin": "spin"; "pulse": "pulse"; "flip": "flip"; "size": "size"; "pull": "pull"; "border": "border"; "inverse": "inverse"; "rotate": "rotate"; "fixedWidth": "fixedWidth"; "transform": "transform"; }, {}, never, never>;
}

//# sourceMappingURL=layers-text.component.d.ts.map